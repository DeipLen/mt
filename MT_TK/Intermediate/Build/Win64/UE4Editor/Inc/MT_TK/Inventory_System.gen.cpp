// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Inventory_System.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInventory_System() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_UInventory_System_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UInventory_System();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
	MT_TK_API UScriptStruct* Z_Construct_UScriptStruct_FInventory_Item();
// End Cross Module References
	DEFINE_FUNCTION(UInventory_System::execChange_Item)
	{
		P_GET_STRUCT(FInventory_Item,Z_Param_FI);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Change_Item(Z_Param_FI);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInventory_System::execCount_Item)
	{
		P_GET_STRUCT(FInventory_Item,Z_Param_FI);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->Count_Item(Z_Param_FI);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInventory_System::execDelete_Item)
	{
		P_GET_STRUCT(FInventory_Item,Z_Param_FI);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Delete_Item(Z_Param_FI);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInventory_System::execFind_Item)
	{
		P_GET_STRUCT(FInventory_Item,Z_Param_FI);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FInventory_Item*)Z_Param__Result=P_THIS->Find_Item(Z_Param_FI);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UInventory_System::execAdd_Item)
	{
		P_GET_STRUCT(FInventory_Item,Z_Param_FI);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Add_Item(Z_Param_FI);
		P_NATIVE_END;
	}
	void UInventory_System::StaticRegisterNativesUInventory_System()
	{
		UClass* Class = UInventory_System::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Add_Item", &UInventory_System::execAdd_Item },
			{ "Change_Item", &UInventory_System::execChange_Item },
			{ "Count_Item", &UInventory_System::execCount_Item },
			{ "Delete_Item", &UInventory_System::execDelete_Item },
			{ "Find_Item", &UInventory_System::execFind_Item },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UInventory_System_Add_Item_Statics
	{
		struct Inventory_System_eventAdd_Item_Parms
		{
			FInventory_Item FI;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInventory_System_Add_Item_Statics::NewProp_FI = { "FI", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventAdd_Item_Parms, FI), Z_Construct_UScriptStruct_FInventory_Item, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInventory_System_Add_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Add_Item_Statics::NewProp_FI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInventory_System_Add_Item_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Inventory_System.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInventory_System_Add_Item_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInventory_System, nullptr, "Add_Item", nullptr, nullptr, sizeof(Inventory_System_eventAdd_Item_Parms), Z_Construct_UFunction_UInventory_System_Add_Item_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Add_Item_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInventory_System_Add_Item_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Add_Item_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInventory_System_Add_Item()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInventory_System_Add_Item_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInventory_System_Change_Item_Statics
	{
		struct Inventory_System_eventChange_Item_Parms
		{
			FInventory_Item FI;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInventory_System_Change_Item_Statics::NewProp_FI = { "FI", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventChange_Item_Parms, FI), Z_Construct_UScriptStruct_FInventory_Item, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInventory_System_Change_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Change_Item_Statics::NewProp_FI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInventory_System_Change_Item_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Inventory_System.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInventory_System_Change_Item_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInventory_System, nullptr, "Change_Item", nullptr, nullptr, sizeof(Inventory_System_eventChange_Item_Parms), Z_Construct_UFunction_UInventory_System_Change_Item_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Change_Item_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInventory_System_Change_Item_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Change_Item_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInventory_System_Change_Item()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInventory_System_Change_Item_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInventory_System_Count_Item_Statics
	{
		struct Inventory_System_eventCount_Item_Parms
		{
			FInventory_Item FI;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FI;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInventory_System_Count_Item_Statics::NewProp_FI = { "FI", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventCount_Item_Parms, FI), Z_Construct_UScriptStruct_FInventory_Item, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UInventory_System_Count_Item_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventCount_Item_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInventory_System_Count_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Count_Item_Statics::NewProp_FI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Count_Item_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInventory_System_Count_Item_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Inventory_System.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInventory_System_Count_Item_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInventory_System, nullptr, "Count_Item", nullptr, nullptr, sizeof(Inventory_System_eventCount_Item_Parms), Z_Construct_UFunction_UInventory_System_Count_Item_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Count_Item_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInventory_System_Count_Item_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Count_Item_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInventory_System_Count_Item()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInventory_System_Count_Item_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInventory_System_Delete_Item_Statics
	{
		struct Inventory_System_eventDelete_Item_Parms
		{
			FInventory_Item FI;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::NewProp_FI = { "FI", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventDelete_Item_Parms, FI), Z_Construct_UScriptStruct_FInventory_Item, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::NewProp_FI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Inventory_System.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInventory_System, nullptr, "Delete_Item", nullptr, nullptr, sizeof(Inventory_System_eventDelete_Item_Parms), Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInventory_System_Delete_Item()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInventory_System_Delete_Item_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UInventory_System_Find_Item_Statics
	{
		struct Inventory_System_eventFind_Item_Parms
		{
			FInventory_Item FI;
			FInventory_Item ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FI;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInventory_System_Find_Item_Statics::NewProp_FI = { "FI", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventFind_Item_Parms, FI), Z_Construct_UScriptStruct_FInventory_Item, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UInventory_System_Find_Item_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Inventory_System_eventFind_Item_Parms, ReturnValue), Z_Construct_UScriptStruct_FInventory_Item, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UInventory_System_Find_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Find_Item_Statics::NewProp_FI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UInventory_System_Find_Item_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UInventory_System_Find_Item_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Inventory_System.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UInventory_System_Find_Item_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UInventory_System, nullptr, "Find_Item", nullptr, nullptr, sizeof(Inventory_System_eventFind_Item_Parms), Z_Construct_UFunction_UInventory_System_Find_Item_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Find_Item_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UInventory_System_Find_Item_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UInventory_System_Find_Item_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UInventory_System_Find_Item()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UInventory_System_Find_Item_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UInventory_System_NoRegister()
	{
		return UInventory_System::StaticClass();
	}
	struct Z_Construct_UClass_UInventory_System_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInventory_System_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UInventory_System_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UInventory_System_Add_Item, "Add_Item" }, // 1515961701
		{ &Z_Construct_UFunction_UInventory_System_Change_Item, "Change_Item" }, // 4211937172
		{ &Z_Construct_UFunction_UInventory_System_Count_Item, "Count_Item" }, // 3370126290
		{ &Z_Construct_UFunction_UInventory_System_Delete_Item, "Delete_Item" }, // 1739053050
		{ &Z_Construct_UFunction_UInventory_System_Find_Item, "Find_Item" }, // 4250783871
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInventory_System_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Inventory_System.h" },
		{ "ModuleRelativePath", "Inventory_System.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInventory_System_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInventory_System>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInventory_System_Statics::ClassParams = {
		&UInventory_System::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UInventory_System_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInventory_System_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInventory_System()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInventory_System_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInventory_System, 2804918341);
	template<> MT_TK_API UClass* StaticClass<UInventory_System>()
	{
		return UInventory_System::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInventory_System(Z_Construct_UClass_UInventory_System, &UInventory_System::StaticClass, TEXT("/Script/MT_TK"), TEXT("UInventory_System"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInventory_System);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
