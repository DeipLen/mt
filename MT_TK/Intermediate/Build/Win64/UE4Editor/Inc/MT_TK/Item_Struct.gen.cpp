// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Item_Struct.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeItem_Struct() {}
// Cross Module References
	MT_TK_API UScriptStruct* Z_Construct_UScriptStruct_FInventory_Item();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
class UScriptStruct* FInventory_Item::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MT_TK_API uint32 Get_Z_Construct_UScriptStruct_FInventory_Item_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FInventory_Item, Z_Construct_UPackage__Script_MT_TK(), TEXT("Inventory_Item"), sizeof(FInventory_Item), Get_Z_Construct_UScriptStruct_FInventory_Item_Hash());
	}
	return Singleton;
}
template<> MT_TK_API UScriptStruct* StaticStruct<FInventory_Item>()
{
	return FInventory_Item::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FInventory_Item(FInventory_Item::StaticStruct, TEXT("/Script/MT_TK"), TEXT("Inventory_Item"), false, nullptr, nullptr);
static struct FScriptStruct_MT_TK_StaticRegisterNativesFInventory_Item
{
	FScriptStruct_MT_TK_StaticRegisterNativesFInventory_Item()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("Inventory_Item")),new UScriptStruct::TCppStructOps<FInventory_Item>);
	}
} ScriptStruct_MT_TK_StaticRegisterNativesFInventory_Item;
	struct Z_Construct_UScriptStruct_FInventory_Item_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Use_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Use_Ref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_Use_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Count_Use;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInventory_Item_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Item_Struct.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FInventory_Item_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FInventory_Item>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Item_Struct.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInventory_Item, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Use_Ref_MetaData[] = {
		{ "ModuleRelativePath", "Item_Struct.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Use_Ref = { "Use_Ref", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInventory_Item, Use_Ref), METADATA_PARAMS(Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Use_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Use_Ref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Count_Use_MetaData[] = {
		{ "ModuleRelativePath", "Item_Struct.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Count_Use = { "Count_Use", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FInventory_Item, Count_Use), METADATA_PARAMS(Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Count_Use_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Count_Use_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FInventory_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Use_Ref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FInventory_Item_Statics::NewProp_Count_Use,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FInventory_Item_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
		nullptr,
		&NewStructOps,
		"Inventory_Item",
		sizeof(FInventory_Item),
		alignof(FInventory_Item),
		Z_Construct_UScriptStruct_FInventory_Item_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInventory_Item_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FInventory_Item_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FInventory_Item_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FInventory_Item()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FInventory_Item_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MT_TK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("Inventory_Item"), sizeof(FInventory_Item), Get_Z_Construct_UScriptStruct_FInventory_Item_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FInventory_Item_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FInventory_Item_Hash() { return 4043681266U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
