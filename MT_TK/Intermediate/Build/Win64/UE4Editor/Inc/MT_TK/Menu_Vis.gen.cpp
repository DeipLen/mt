// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Menu_Vis.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMenu_Vis() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_UMenu_Vis_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UMenu_Vis();
	MT_TK_API UClass* Z_Construct_UClass_UMenu();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	void UMenu_Vis::StaticRegisterNativesUMenu_Vis()
	{
	}
	UClass* Z_Construct_UClass_UMenu_Vis_NoRegister()
	{
		return UMenu_Vis::StaticClass();
	}
	struct Z_Construct_UClass_UMenu_Vis_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMenu_Vis_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMenu,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Vis_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Menu_Vis.h" },
		{ "ModuleRelativePath", "Menu_Vis.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMenu_Vis_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMenu_Vis>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMenu_Vis_Statics::ClassParams = {
		&UMenu_Vis::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMenu_Vis_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Vis_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMenu_Vis()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMenu_Vis_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMenu_Vis, 1210781624);
	template<> MT_TK_API UClass* StaticClass<UMenu_Vis>()
	{
		return UMenu_Vis::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMenu_Vis(Z_Construct_UClass_UMenu_Vis, &UMenu_Vis::StaticClass, TEXT("/Script/MT_TK"), TEXT("UMenu_Vis"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMenu_Vis);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
