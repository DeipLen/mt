// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_MT_TKPlayerController_generated_h
#error "MT_TKPlayerController.generated.h already included, missing '#pragma once' in MT_TKPlayerController.h"
#endif
#define MT_TK_MT_TKPlayerController_generated_h

#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_SPARSE_DATA
#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execNAM_3); \
	DECLARE_FUNCTION(execNAM_2); \
	DECLARE_FUNCTION(execNAM_1);


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execNAM_3); \
	DECLARE_FUNCTION(execNAM_2); \
	DECLARE_FUNCTION(execNAM_1);


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMT_TKPlayerController(); \
	friend struct Z_Construct_UClass_AMT_TKPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMT_TKPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AMT_TKPlayerController)


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMT_TKPlayerController(); \
	friend struct Z_Construct_UClass_AMT_TKPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMT_TKPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AMT_TKPlayerController)


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMT_TKPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMT_TKPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMT_TKPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMT_TKPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMT_TKPlayerController(AMT_TKPlayerController&&); \
	NO_API AMT_TKPlayerController(const AMT_TKPlayerController&); \
public:


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMT_TKPlayerController(AMT_TKPlayerController&&); \
	NO_API AMT_TKPlayerController(const AMT_TKPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMT_TKPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMT_TKPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMT_TKPlayerController)


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_9_PROLOG
#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_SPARSE_DATA \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_INCLASS \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_SPARSE_DATA \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_MT_TKPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class AMT_TKPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_MT_TKPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
