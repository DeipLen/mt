// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Use_Item_Three.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUse_Item_Three() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AUse_Item_Three_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AUse_Item_Three();
	MT_TK_API UClass* Z_Construct_UClass_AUse_Item();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	void AUse_Item_Three::StaticRegisterNativesAUse_Item_Three()
	{
	}
	UClass* Z_Construct_UClass_AUse_Item_Three_NoRegister()
	{
		return AUse_Item_Three::StaticClass();
	}
	struct Z_Construct_UClass_AUse_Item_Three_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUse_Item_Three_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AUse_Item,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUse_Item_Three_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Use_Item_Three.h" },
		{ "ModuleRelativePath", "Use_Item_Three.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUse_Item_Three_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUse_Item_Three>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUse_Item_Three_Statics::ClassParams = {
		&AUse_Item_Three::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AUse_Item_Three_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUse_Item_Three_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUse_Item_Three()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUse_Item_Three_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUse_Item_Three, 1271958260);
	template<> MT_TK_API UClass* StaticClass<AUse_Item_Three>()
	{
		return AUse_Item_Three::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUse_Item_Three(Z_Construct_UClass_AUse_Item_Three, &AUse_Item_Three::StaticClass, TEXT("/Script/MT_TK"), TEXT("AUse_Item_Three"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUse_Item_Three);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
