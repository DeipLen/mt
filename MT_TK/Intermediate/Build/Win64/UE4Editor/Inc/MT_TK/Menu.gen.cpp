// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Menu.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMenu() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_UMenu_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UMenu();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
	UMG_API UClass* Z_Construct_UClass_UButton_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKCharacter_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UOverlay_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UScrollBox_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AShop_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UWrapBox_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMenu::execGet_Shop_Ref)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AShop**)Z_Param__Result=P_THIS->Get_Shop_Ref();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_Char_Ref)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AMT_TKCharacter**)Z_Param__Result=P_THIS->Get_Char_Ref();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_WrapBoxC)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWrapBox**)Z_Param__Result=P_THIS->Get_WrapBoxC();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_ScrollBoxC)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UScrollBox**)Z_Param__Result=P_THIS->Get_ScrollBoxC();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_OverlayC)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOverlay**)Z_Param__Result=P_THIS->Get_OverlayC();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_Image_Mid)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UImage**)Z_Param__Result=P_THIS->Get_Image_Mid();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_Image_Back)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UImage**)Z_Param__Result=P_THIS->Get_Image_Back();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execGet_Button_Exit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UButton**)Z_Param__Result=P_THIS->Get_Button_Exit();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execSet_Shop_Ref)
	{
		P_GET_OBJECT(AShop,Z_Param_AS);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Shop_Ref(Z_Param_AS);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execSet_Char_Ref)
	{
		P_GET_OBJECT(AMT_TKCharacter,Z_Param_AM);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Char_Ref(Z_Param_AM);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execStartButtonClicked)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartButtonClicked();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMenu::execAddUMGItem)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Text);
		P_GET_PROPERTY(FStrProperty,Z_Param_Link);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddUMGItem(Z_Param_Text,Z_Param_Link);
		P_NATIVE_END;
	}
	void UMenu::StaticRegisterNativesUMenu()
	{
		UClass* Class = UMenu::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddUMGItem", &UMenu::execAddUMGItem },
			{ "Get_Button_Exit", &UMenu::execGet_Button_Exit },
			{ "Get_Char_Ref", &UMenu::execGet_Char_Ref },
			{ "Get_Image_Back", &UMenu::execGet_Image_Back },
			{ "Get_Image_Mid", &UMenu::execGet_Image_Mid },
			{ "Get_OverlayC", &UMenu::execGet_OverlayC },
			{ "Get_ScrollBoxC", &UMenu::execGet_ScrollBoxC },
			{ "Get_Shop_Ref", &UMenu::execGet_Shop_Ref },
			{ "Get_WrapBoxC", &UMenu::execGet_WrapBoxC },
			{ "Set_Char_Ref", &UMenu::execSet_Char_Ref },
			{ "Set_Shop_Ref", &UMenu::execSet_Shop_Ref },
			{ "StartButtonClicked", &UMenu::execStartButtonClicked },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMenu_AddUMGItem_Statics
	{
		struct Menu_eventAddUMGItem_Parms
		{
			FString Text;
			FString Link;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Text;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Link;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMenu_AddUMGItem_Statics::NewProp_Text = { "Text", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventAddUMGItem_Parms, Text), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMenu_AddUMGItem_Statics::NewProp_Link = { "Link", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventAddUMGItem_Parms, Link), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_AddUMGItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_AddUMGItem_Statics::NewProp_Text,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_AddUMGItem_Statics::NewProp_Link,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_AddUMGItem_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_AddUMGItem_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "AddUMGItem", nullptr, nullptr, sizeof(Menu_eventAddUMGItem_Parms), Z_Construct_UFunction_UMenu_AddUMGItem_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_AddUMGItem_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_AddUMGItem_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_AddUMGItem_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_AddUMGItem()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_AddUMGItem_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics
	{
		struct Menu_eventGet_Button_Exit_Parms
		{
			UButton* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_Button_Exit_Parms, ReturnValue), Z_Construct_UClass_UButton_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_Button_Exit", nullptr, nullptr, sizeof(Menu_eventGet_Button_Exit_Parms), Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_Button_Exit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_Button_Exit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics
	{
		struct Menu_eventGet_Char_Ref_Parms
		{
			AMT_TKCharacter* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_Char_Ref_Parms, ReturnValue), Z_Construct_UClass_AMT_TKCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_Char_Ref", nullptr, nullptr, sizeof(Menu_eventGet_Char_Ref_Parms), Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_Char_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_Char_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_Image_Back_Statics
	{
		struct Menu_eventGet_Image_Back_Parms
		{
			UImage* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_Image_Back_Parms, ReturnValue), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_Image_Back", nullptr, nullptr, sizeof(Menu_eventGet_Image_Back_Parms), Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_Image_Back()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_Image_Back_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics
	{
		struct Menu_eventGet_Image_Mid_Parms
		{
			UImage* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_Image_Mid_Parms, ReturnValue), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_Image_Mid", nullptr, nullptr, sizeof(Menu_eventGet_Image_Mid_Parms), Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_Image_Mid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_Image_Mid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_OverlayC_Statics
	{
		struct Menu_eventGet_OverlayC_Parms
		{
			UOverlay* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_OverlayC_Parms, ReturnValue), Z_Construct_UClass_UOverlay_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_OverlayC", nullptr, nullptr, sizeof(Menu_eventGet_OverlayC_Parms), Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_OverlayC()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_OverlayC_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics
	{
		struct Menu_eventGet_ScrollBoxC_Parms
		{
			UScrollBox* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_ScrollBoxC_Parms, ReturnValue), Z_Construct_UClass_UScrollBox_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_ScrollBoxC", nullptr, nullptr, sizeof(Menu_eventGet_ScrollBoxC_Parms), Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_ScrollBoxC()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_ScrollBoxC_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics
	{
		struct Menu_eventGet_Shop_Ref_Parms
		{
			AShop* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_Shop_Ref_Parms, ReturnValue), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_Shop_Ref", nullptr, nullptr, sizeof(Menu_eventGet_Shop_Ref_Parms), Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_Shop_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_Shop_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics
	{
		struct Menu_eventGet_WrapBoxC_Parms
		{
			UWrapBox* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventGet_WrapBoxC_Parms, ReturnValue), Z_Construct_UClass_UWrapBox_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Get_WrapBoxC", nullptr, nullptr, sizeof(Menu_eventGet_WrapBoxC_Parms), Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Get_WrapBoxC()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Get_WrapBoxC_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics
	{
		struct Menu_eventSet_Char_Ref_Parms
		{
			AMT_TKCharacter* AM;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AM;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::NewProp_AM = { "AM", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventSet_Char_Ref_Parms, AM), Z_Construct_UClass_AMT_TKCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::NewProp_AM,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Set_Char_Ref", nullptr, nullptr, sizeof(Menu_eventSet_Char_Ref_Parms), Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Set_Char_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Set_Char_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics
	{
		struct Menu_eventSet_Shop_Ref_Parms
		{
			AShop* AS;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::NewProp_AS = { "AS", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Menu_eventSet_Shop_Ref_Parms, AS), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::NewProp_AS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "Set_Shop_Ref", nullptr, nullptr, sizeof(Menu_eventSet_Shop_Ref_Parms), Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_Set_Shop_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_Set_Shop_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMenu_StartButtonClicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMenu_StartButtonClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMenu_StartButtonClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMenu, nullptr, "StartButtonClicked", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMenu_StartButtonClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMenu_StartButtonClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMenu_StartButtonClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMenu_StartButtonClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMenu_NoRegister()
	{
		return UMenu::StaticClass();
	}
	struct Z_Construct_UClass_UMenu_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Button_Exit_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Button_Exit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Image_Back_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Image_Back;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Image_Mid_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Image_Mid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayC_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayC;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScrollBoxC_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScrollBoxC;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WrapBoxC_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WrapBoxC;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Char_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Char_Ref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shop_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Shop_Ref;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMenu_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMenu_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMenu_AddUMGItem, "AddUMGItem" }, // 2702239286
		{ &Z_Construct_UFunction_UMenu_Get_Button_Exit, "Get_Button_Exit" }, // 2008946143
		{ &Z_Construct_UFunction_UMenu_Get_Char_Ref, "Get_Char_Ref" }, // 1791056308
		{ &Z_Construct_UFunction_UMenu_Get_Image_Back, "Get_Image_Back" }, // 31984456
		{ &Z_Construct_UFunction_UMenu_Get_Image_Mid, "Get_Image_Mid" }, // 1802247607
		{ &Z_Construct_UFunction_UMenu_Get_OverlayC, "Get_OverlayC" }, // 2847470054
		{ &Z_Construct_UFunction_UMenu_Get_ScrollBoxC, "Get_ScrollBoxC" }, // 289475435
		{ &Z_Construct_UFunction_UMenu_Get_Shop_Ref, "Get_Shop_Ref" }, // 2039348546
		{ &Z_Construct_UFunction_UMenu_Get_WrapBoxC, "Get_WrapBoxC" }, // 3289201698
		{ &Z_Construct_UFunction_UMenu_Set_Char_Ref, "Set_Char_Ref" }, // 3202282409
		{ &Z_Construct_UFunction_UMenu_Set_Shop_Ref, "Set_Shop_Ref" }, // 231824009
		{ &Z_Construct_UFunction_UMenu_StartButtonClicked, "StartButtonClicked" }, // 2029598408
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Menu.h" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_Button_Exit_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_Button_Exit = { "Button_Exit", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, Button_Exit), Z_Construct_UClass_UButton_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_Button_Exit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_Button_Exit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_Image_Back_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_Image_Back = { "Image_Back", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, Image_Back), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_Image_Back_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_Image_Back_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_Image_Mid_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_Image_Mid = { "Image_Mid", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, Image_Mid), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_Image_Mid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_Image_Mid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_OverlayC_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_OverlayC = { "OverlayC", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, OverlayC), Z_Construct_UClass_UOverlay_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_OverlayC_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_OverlayC_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_ScrollBoxC_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_ScrollBoxC = { "ScrollBoxC", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, ScrollBoxC), Z_Construct_UClass_UScrollBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_ScrollBoxC_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_ScrollBoxC_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_WrapBoxC_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_WrapBoxC = { "WrapBoxC", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, WrapBoxC), Z_Construct_UClass_UWrapBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_WrapBoxC_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_WrapBoxC_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_Char_Ref_MetaData[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_Char_Ref = { "Char_Ref", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, Char_Ref), Z_Construct_UClass_AMT_TKCharacter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_Char_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_Char_Ref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMenu_Statics::NewProp_Shop_Ref_MetaData[] = {
		{ "ModuleRelativePath", "Menu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMenu_Statics::NewProp_Shop_Ref = { "Shop_Ref", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMenu, Shop_Ref), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::NewProp_Shop_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::NewProp_Shop_Ref_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMenu_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_Button_Exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_Image_Back,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_Image_Mid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_OverlayC,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_ScrollBoxC,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_WrapBoxC,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_Char_Ref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMenu_Statics::NewProp_Shop_Ref,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMenu_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMenu>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMenu_Statics::ClassParams = {
		&UMenu::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMenu_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMenu_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMenu_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMenu()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMenu_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMenu, 822264545);
	template<> MT_TK_API UClass* StaticClass<UMenu>()
	{
		return UMenu::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMenu(Z_Construct_UClass_UMenu, &UMenu::StaticClass, TEXT("/Script/MT_TK"), TEXT("UMenu"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMenu);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
