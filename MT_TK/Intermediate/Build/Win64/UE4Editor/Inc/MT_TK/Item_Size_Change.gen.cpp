// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Item_Size_Change.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeItem_Size_Change() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AItem_Size_Change_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AItem_Size_Change();
	MT_TK_API UClass* Z_Construct_UClass_AItem();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	void AItem_Size_Change::StaticRegisterNativesAItem_Size_Change()
	{
	}
	UClass* Z_Construct_UClass_AItem_Size_Change_NoRegister()
	{
		return AItem_Size_Change::StaticClass();
	}
	struct Z_Construct_UClass_AItem_Size_Change_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AItem_Size_Change_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AItem,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AItem_Size_Change_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Item_Size_Change.h" },
		{ "ModuleRelativePath", "Item_Size_Change.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AItem_Size_Change_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AItem_Size_Change>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AItem_Size_Change_Statics::ClassParams = {
		&AItem_Size_Change::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AItem_Size_Change_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AItem_Size_Change_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AItem_Size_Change()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AItem_Size_Change_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AItem_Size_Change, 145992965);
	template<> MT_TK_API UClass* StaticClass<AItem_Size_Change>()
	{
		return AItem_Size_Change::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AItem_Size_Change(Z_Construct_UClass_AItem_Size_Change, &AItem_Size_Change::StaticClass, TEXT("/Script/MT_TK"), TEXT("AItem_Size_Change"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AItem_Size_Change);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
