// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AItem;
class UMenu;
class AMT_TKCharacter;
class AShop;
class UTextBlock;
class UCanvasPanel;
class UImage;
class UOverlay;
class UButton;
#ifdef MT_TK_UMG_Item_generated_h
#error "UMG_Item.generated.h already included, missing '#pragma once' in UMG_Item.h"
#endif
#define MT_TK_UMG_Item_generated_h

#define MT_TK_Source_MT_TK_UMG_Item_h_18_SPARSE_DATA
#define MT_TK_Source_MT_TK_UMG_Item_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGet_Actor_To_Spawn); \
	DECLARE_FUNCTION(execGet_Menu_Ref); \
	DECLARE_FUNCTION(execGet_Char_Ref); \
	DECLARE_FUNCTION(execGet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_TextBlock_Description); \
	DECLARE_FUNCTION(execGet_CanvasPanelC); \
	DECLARE_FUNCTION(execGet_Image_Mid); \
	DECLARE_FUNCTION(execGet_Image_Back); \
	DECLARE_FUNCTION(execGet_OverlayC); \
	DECLARE_FUNCTION(execGet_Button_Buy); \
	DECLARE_FUNCTION(execSet_Actor_To_Spawn); \
	DECLARE_FUNCTION(execSet_Menu_Ref); \
	DECLARE_FUNCTION(execSet_Char_Ref); \
	DECLARE_FUNCTION(execSet_Shop_Ref); \
	DECLARE_FUNCTION(execSpawn_Actors); \
	DECLARE_FUNCTION(execStartButtonClicked);


#define MT_TK_Source_MT_TK_UMG_Item_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGet_Actor_To_Spawn); \
	DECLARE_FUNCTION(execGet_Menu_Ref); \
	DECLARE_FUNCTION(execGet_Char_Ref); \
	DECLARE_FUNCTION(execGet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_TextBlock_Description); \
	DECLARE_FUNCTION(execGet_CanvasPanelC); \
	DECLARE_FUNCTION(execGet_Image_Mid); \
	DECLARE_FUNCTION(execGet_Image_Back); \
	DECLARE_FUNCTION(execGet_OverlayC); \
	DECLARE_FUNCTION(execGet_Button_Buy); \
	DECLARE_FUNCTION(execSet_Actor_To_Spawn); \
	DECLARE_FUNCTION(execSet_Menu_Ref); \
	DECLARE_FUNCTION(execSet_Char_Ref); \
	DECLARE_FUNCTION(execSet_Shop_Ref); \
	DECLARE_FUNCTION(execSpawn_Actors); \
	DECLARE_FUNCTION(execStartButtonClicked);


#define MT_TK_Source_MT_TK_UMG_Item_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUMG_Item(); \
	friend struct Z_Construct_UClass_UUMG_Item_Statics; \
public: \
	DECLARE_CLASS(UUMG_Item, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UUMG_Item)


#define MT_TK_Source_MT_TK_UMG_Item_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUUMG_Item(); \
	friend struct Z_Construct_UClass_UUMG_Item_Statics; \
public: \
	DECLARE_CLASS(UUMG_Item, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UUMG_Item)


#define MT_TK_Source_MT_TK_UMG_Item_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUMG_Item(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUMG_Item) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUMG_Item); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUMG_Item); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUMG_Item(UUMG_Item&&); \
	NO_API UUMG_Item(const UUMG_Item&); \
public:


#define MT_TK_Source_MT_TK_UMG_Item_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUMG_Item(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUMG_Item(UUMG_Item&&); \
	NO_API UUMG_Item(const UUMG_Item&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUMG_Item); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUMG_Item); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUMG_Item)


#define MT_TK_Source_MT_TK_UMG_Item_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Button_Buy() { return STRUCT_OFFSET(UUMG_Item, Button_Buy); } \
	FORCEINLINE static uint32 __PPO__OverlayC() { return STRUCT_OFFSET(UUMG_Item, OverlayC); } \
	FORCEINLINE static uint32 __PPO__Image_Back() { return STRUCT_OFFSET(UUMG_Item, Image_Back); } \
	FORCEINLINE static uint32 __PPO__Image_Mid() { return STRUCT_OFFSET(UUMG_Item, Image_Mid); } \
	FORCEINLINE static uint32 __PPO__CanvasPanelC() { return STRUCT_OFFSET(UUMG_Item, CanvasPanelC); } \
	FORCEINLINE static uint32 __PPO__TextBlock_Description() { return STRUCT_OFFSET(UUMG_Item, TextBlock_Description); } \
	FORCEINLINE static uint32 __PPO__Shop_Ref() { return STRUCT_OFFSET(UUMG_Item, Shop_Ref); } \
	FORCEINLINE static uint32 __PPO__Char_Ref() { return STRUCT_OFFSET(UUMG_Item, Char_Ref); } \
	FORCEINLINE static uint32 __PPO__Menu_Ref() { return STRUCT_OFFSET(UUMG_Item, Menu_Ref); } \
	FORCEINLINE static uint32 __PPO__Actor_To_Spawn() { return STRUCT_OFFSET(UUMG_Item, Actor_To_Spawn); }


#define MT_TK_Source_MT_TK_UMG_Item_h_15_PROLOG
#define MT_TK_Source_MT_TK_UMG_Item_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_UMG_Item_h_18_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_UMG_Item_h_18_SPARSE_DATA \
	MT_TK_Source_MT_TK_UMG_Item_h_18_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_UMG_Item_h_18_INCLASS \
	MT_TK_Source_MT_TK_UMG_Item_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_UMG_Item_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_UMG_Item_h_18_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_UMG_Item_h_18_SPARSE_DATA \
	MT_TK_Source_MT_TK_UMG_Item_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_UMG_Item_h_18_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_UMG_Item_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class UUMG_Item>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_UMG_Item_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
