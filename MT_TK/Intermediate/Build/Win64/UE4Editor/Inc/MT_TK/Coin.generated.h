// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_Coin_generated_h
#error "Coin.generated.h already included, missing '#pragma once' in Coin.h"
#endif
#define MT_TK_Coin_generated_h

#define MT_TK_Source_MT_TK_Coin_h_14_SPARSE_DATA
#define MT_TK_Source_MT_TK_Coin_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGet_Cost); \
	DECLARE_FUNCTION(execSet_Cost);


#define MT_TK_Source_MT_TK_Coin_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGet_Cost); \
	DECLARE_FUNCTION(execSet_Cost);


#define MT_TK_Source_MT_TK_Coin_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACoin(); \
	friend struct Z_Construct_UClass_ACoin_Statics; \
public: \
	DECLARE_CLASS(ACoin, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(ACoin) \
	virtual UObject* _getUObject() const override { return const_cast<ACoin*>(this); }


#define MT_TK_Source_MT_TK_Coin_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACoin(); \
	friend struct Z_Construct_UClass_ACoin_Statics; \
public: \
	DECLARE_CLASS(ACoin, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(ACoin) \
	virtual UObject* _getUObject() const override { return const_cast<ACoin*>(this); }


#define MT_TK_Source_MT_TK_Coin_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACoin(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACoin) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACoin); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACoin); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACoin(ACoin&&); \
	NO_API ACoin(const ACoin&); \
public:


#define MT_TK_Source_MT_TK_Coin_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACoin(ACoin&&); \
	NO_API ACoin(const ACoin&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACoin); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACoin); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACoin)


#define MT_TK_Source_MT_TK_Coin_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Coin_Mesh() { return STRUCT_OFFSET(ACoin, Coin_Mesh); } \
	FORCEINLINE static uint32 __PPO__Coin_Component() { return STRUCT_OFFSET(ACoin, Coin_Component); } \
	FORCEINLINE static uint32 __PPO__Cost() { return STRUCT_OFFSET(ACoin, Cost); }


#define MT_TK_Source_MT_TK_Coin_h_11_PROLOG
#define MT_TK_Source_MT_TK_Coin_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Coin_h_14_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Coin_h_14_SPARSE_DATA \
	MT_TK_Source_MT_TK_Coin_h_14_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Coin_h_14_INCLASS \
	MT_TK_Source_MT_TK_Coin_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Coin_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Coin_h_14_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Coin_h_14_SPARSE_DATA \
	MT_TK_Source_MT_TK_Coin_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Coin_h_14_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Coin_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class ACoin>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Coin_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
