// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_Item_Size_Change_generated_h
#error "Item_Size_Change.generated.h already included, missing '#pragma once' in Item_Size_Change.h"
#endif
#define MT_TK_Item_Size_Change_generated_h

#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_SPARSE_DATA
#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_RPC_WRAPPERS
#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAItem_Size_Change(); \
	friend struct Z_Construct_UClass_AItem_Size_Change_Statics; \
public: \
	DECLARE_CLASS(AItem_Size_Change, AItem, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AItem_Size_Change)


#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAItem_Size_Change(); \
	friend struct Z_Construct_UClass_AItem_Size_Change_Statics; \
public: \
	DECLARE_CLASS(AItem_Size_Change, AItem, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AItem_Size_Change)


#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AItem_Size_Change(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AItem_Size_Change) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItem_Size_Change); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItem_Size_Change); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItem_Size_Change(AItem_Size_Change&&); \
	NO_API AItem_Size_Change(const AItem_Size_Change&); \
public:


#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItem_Size_Change(AItem_Size_Change&&); \
	NO_API AItem_Size_Change(const AItem_Size_Change&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItem_Size_Change); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItem_Size_Change); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AItem_Size_Change)


#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_Item_Size_Change_h_12_PROLOG
#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_INCLASS \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Item_Size_Change_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Item_Size_Change_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class AItem_Size_Change>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Item_Size_Change_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
