// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Item_Three.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeItem_Three() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AItem_Three_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AItem_Three();
	MT_TK_API UClass* Z_Construct_UClass_AItem();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	void AItem_Three::StaticRegisterNativesAItem_Three()
	{
	}
	UClass* Z_Construct_UClass_AItem_Three_NoRegister()
	{
		return AItem_Three::StaticClass();
	}
	struct Z_Construct_UClass_AItem_Three_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AItem_Three_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AItem,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AItem_Three_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Item_Three.h" },
		{ "ModuleRelativePath", "Item_Three.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AItem_Three_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AItem_Three>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AItem_Three_Statics::ClassParams = {
		&AItem_Three::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AItem_Three_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AItem_Three_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AItem_Three()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AItem_Three_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AItem_Three, 131362009);
	template<> MT_TK_API UClass* StaticClass<AItem_Three>()
	{
		return AItem_Three::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AItem_Three(Z_Construct_UClass_AItem_Three, &AItem_Three::StaticClass, TEXT("/Script/MT_TK"), TEXT("AItem_Three"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AItem_Three);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
