// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/UMG_Item.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUMG_Item() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_UUMG_Item_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UUMG_Item();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MT_TK_API UClass* Z_Construct_UClass_AItem_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UButton_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UCanvasPanel_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKCharacter_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UMenu_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UOverlay_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AShop_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UUMG_Item::execGet_Actor_To_Spawn)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSubclassOf<AItem> *)Z_Param__Result=P_THIS->Get_Actor_To_Spawn();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_Menu_Ref)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMenu**)Z_Param__Result=P_THIS->Get_Menu_Ref();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_Char_Ref)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AMT_TKCharacter**)Z_Param__Result=P_THIS->Get_Char_Ref();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_Shop_Ref)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AShop**)Z_Param__Result=P_THIS->Get_Shop_Ref();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_TextBlock_Description)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTextBlock**)Z_Param__Result=P_THIS->Get_TextBlock_Description();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_CanvasPanelC)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCanvasPanel**)Z_Param__Result=P_THIS->Get_CanvasPanelC();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_Image_Mid)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UImage**)Z_Param__Result=P_THIS->Get_Image_Mid();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_Image_Back)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UImage**)Z_Param__Result=P_THIS->Get_Image_Back();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_OverlayC)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOverlay**)Z_Param__Result=P_THIS->Get_OverlayC();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execGet_Button_Buy)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UButton**)Z_Param__Result=P_THIS->Get_Button_Buy();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execSet_Actor_To_Spawn)
	{
		P_GET_OBJECT(UClass,Z_Param_TS);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Actor_To_Spawn(Z_Param_TS);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execSet_Menu_Ref)
	{
		P_GET_OBJECT(UMenu,Z_Param_UM);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Menu_Ref(Z_Param_UM);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execSet_Char_Ref)
	{
		P_GET_OBJECT(AMT_TKCharacter,Z_Param_AM);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Char_Ref(Z_Param_AM);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execSet_Shop_Ref)
	{
		P_GET_OBJECT(AShop,Z_Param_AS);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Shop_Ref(Z_Param_AS);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execSpawn_Actors)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Spawn_Actors();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUMG_Item::execStartButtonClicked)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartButtonClicked();
		P_NATIVE_END;
	}
	void UUMG_Item::StaticRegisterNativesUUMG_Item()
	{
		UClass* Class = UUMG_Item::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Get_Actor_To_Spawn", &UUMG_Item::execGet_Actor_To_Spawn },
			{ "Get_Button_Buy", &UUMG_Item::execGet_Button_Buy },
			{ "Get_CanvasPanelC", &UUMG_Item::execGet_CanvasPanelC },
			{ "Get_Char_Ref", &UUMG_Item::execGet_Char_Ref },
			{ "Get_Image_Back", &UUMG_Item::execGet_Image_Back },
			{ "Get_Image_Mid", &UUMG_Item::execGet_Image_Mid },
			{ "Get_Menu_Ref", &UUMG_Item::execGet_Menu_Ref },
			{ "Get_OverlayC", &UUMG_Item::execGet_OverlayC },
			{ "Get_Shop_Ref", &UUMG_Item::execGet_Shop_Ref },
			{ "Get_TextBlock_Description", &UUMG_Item::execGet_TextBlock_Description },
			{ "Set_Actor_To_Spawn", &UUMG_Item::execSet_Actor_To_Spawn },
			{ "Set_Char_Ref", &UUMG_Item::execSet_Char_Ref },
			{ "Set_Menu_Ref", &UUMG_Item::execSet_Menu_Ref },
			{ "Set_Shop_Ref", &UUMG_Item::execSet_Shop_Ref },
			{ "Spawn_Actors", &UUMG_Item::execSpawn_Actors },
			{ "StartButtonClicked", &UUMG_Item::execStartButtonClicked },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics
	{
		struct UMG_Item_eventGet_Actor_To_Spawn_Parms
		{
			TSubclassOf<AItem>  ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Actor_To_Spawn_Parms, ReturnValue), Z_Construct_UClass_AItem_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Actor_To_Spawn", nullptr, nullptr, sizeof(UMG_Item_eventGet_Actor_To_Spawn_Parms), Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics
	{
		struct UMG_Item_eventGet_Button_Buy_Parms
		{
			UButton* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Button_Buy_Parms, ReturnValue), Z_Construct_UClass_UButton_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Button_Buy", nullptr, nullptr, sizeof(UMG_Item_eventGet_Button_Buy_Parms), Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Button_Buy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Button_Buy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics
	{
		struct UMG_Item_eventGet_CanvasPanelC_Parms
		{
			UCanvasPanel* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_CanvasPanelC_Parms, ReturnValue), Z_Construct_UClass_UCanvasPanel_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_CanvasPanelC", nullptr, nullptr, sizeof(UMG_Item_eventGet_CanvasPanelC_Parms), Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics
	{
		struct UMG_Item_eventGet_Char_Ref_Parms
		{
			AMT_TKCharacter* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Char_Ref_Parms, ReturnValue), Z_Construct_UClass_AMT_TKCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Char_Ref", nullptr, nullptr, sizeof(UMG_Item_eventGet_Char_Ref_Parms), Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Char_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Char_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics
	{
		struct UMG_Item_eventGet_Image_Back_Parms
		{
			UImage* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Image_Back_Parms, ReturnValue), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Image_Back", nullptr, nullptr, sizeof(UMG_Item_eventGet_Image_Back_Parms), Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Image_Back()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Image_Back_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics
	{
		struct UMG_Item_eventGet_Image_Mid_Parms
		{
			UImage* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Image_Mid_Parms, ReturnValue), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Image_Mid", nullptr, nullptr, sizeof(UMG_Item_eventGet_Image_Mid_Parms), Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Image_Mid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Image_Mid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics
	{
		struct UMG_Item_eventGet_Menu_Ref_Parms
		{
			UMenu* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Menu_Ref_Parms, ReturnValue), Z_Construct_UClass_UMenu_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Menu_Ref", nullptr, nullptr, sizeof(UMG_Item_eventGet_Menu_Ref_Parms), Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics
	{
		struct UMG_Item_eventGet_OverlayC_Parms
		{
			UOverlay* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_OverlayC_Parms, ReturnValue), Z_Construct_UClass_UOverlay_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_OverlayC", nullptr, nullptr, sizeof(UMG_Item_eventGet_OverlayC_Parms), Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_OverlayC()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_OverlayC_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics
	{
		struct UMG_Item_eventGet_Shop_Ref_Parms
		{
			AShop* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_Shop_Ref_Parms, ReturnValue), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_Shop_Ref", nullptr, nullptr, sizeof(UMG_Item_eventGet_Shop_Ref_Parms), Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics
	{
		struct UMG_Item_eventGet_TextBlock_Description_Parms
		{
			UTextBlock* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventGet_TextBlock_Description_Parms, ReturnValue), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Get_TextBlock_Description", nullptr, nullptr, sizeof(UMG_Item_eventGet_TextBlock_Description_Parms), Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics
	{
		struct UMG_Item_eventSet_Actor_To_Spawn_Parms
		{
			TSubclassOf<AItem>  TS;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_TS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::NewProp_TS = { "TS", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventSet_Actor_To_Spawn_Parms, TS), Z_Construct_UClass_AItem_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::NewProp_TS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Set_Actor_To_Spawn", nullptr, nullptr, sizeof(UMG_Item_eventSet_Actor_To_Spawn_Parms), Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics
	{
		struct UMG_Item_eventSet_Char_Ref_Parms
		{
			AMT_TKCharacter* AM;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AM;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::NewProp_AM = { "AM", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventSet_Char_Ref_Parms, AM), Z_Construct_UClass_AMT_TKCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::NewProp_AM,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Set_Char_Ref", nullptr, nullptr, sizeof(UMG_Item_eventSet_Char_Ref_Parms), Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Set_Char_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Set_Char_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics
	{
		struct UMG_Item_eventSet_Menu_Ref_Parms
		{
			UMenu* UM;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UM_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UM;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::NewProp_UM_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::NewProp_UM = { "UM", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventSet_Menu_Ref_Parms, UM), Z_Construct_UClass_UMenu_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::NewProp_UM_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::NewProp_UM_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::NewProp_UM,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Set_Menu_Ref", nullptr, nullptr, sizeof(UMG_Item_eventSet_Menu_Ref_Parms), Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics
	{
		struct UMG_Item_eventSet_Shop_Ref_Parms
		{
			AShop* AS;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::NewProp_AS = { "AS", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMG_Item_eventSet_Shop_Ref_Parms, AS), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::NewProp_AS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Set_Shop_Ref", nullptr, nullptr, sizeof(UMG_Item_eventSet_Shop_Ref_Parms), Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_Spawn_Actors_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_Spawn_Actors_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_Spawn_Actors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "Spawn_Actors", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_Spawn_Actors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_Spawn_Actors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_Spawn_Actors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_Spawn_Actors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUMG_Item_StartButtonClicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUMG_Item_StartButtonClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUMG_Item_StartButtonClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUMG_Item, nullptr, "StartButtonClicked", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUMG_Item_StartButtonClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUMG_Item_StartButtonClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUMG_Item_StartButtonClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUMG_Item_StartButtonClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUMG_Item_NoRegister()
	{
		return UUMG_Item::StaticClass();
	}
	struct Z_Construct_UClass_UUMG_Item_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Button_Buy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Button_Buy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayC_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayC;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Image_Back_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Image_Back;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Image_Mid_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Image_Mid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanvasPanelC_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CanvasPanelC;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextBlock_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextBlock_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shop_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Shop_Ref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Char_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Char_Ref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Menu_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Menu_Ref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actor_To_Spawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Actor_To_Spawn;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUMG_Item_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUMG_Item_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUMG_Item_Get_Actor_To_Spawn, "Get_Actor_To_Spawn" }, // 803924400
		{ &Z_Construct_UFunction_UUMG_Item_Get_Button_Buy, "Get_Button_Buy" }, // 1573810028
		{ &Z_Construct_UFunction_UUMG_Item_Get_CanvasPanelC, "Get_CanvasPanelC" }, // 2567786658
		{ &Z_Construct_UFunction_UUMG_Item_Get_Char_Ref, "Get_Char_Ref" }, // 3703666886
		{ &Z_Construct_UFunction_UUMG_Item_Get_Image_Back, "Get_Image_Back" }, // 3380035150
		{ &Z_Construct_UFunction_UUMG_Item_Get_Image_Mid, "Get_Image_Mid" }, // 1891072351
		{ &Z_Construct_UFunction_UUMG_Item_Get_Menu_Ref, "Get_Menu_Ref" }, // 980720249
		{ &Z_Construct_UFunction_UUMG_Item_Get_OverlayC, "Get_OverlayC" }, // 2740728874
		{ &Z_Construct_UFunction_UUMG_Item_Get_Shop_Ref, "Get_Shop_Ref" }, // 2130051918
		{ &Z_Construct_UFunction_UUMG_Item_Get_TextBlock_Description, "Get_TextBlock_Description" }, // 2698104902
		{ &Z_Construct_UFunction_UUMG_Item_Set_Actor_To_Spawn, "Set_Actor_To_Spawn" }, // 433889435
		{ &Z_Construct_UFunction_UUMG_Item_Set_Char_Ref, "Set_Char_Ref" }, // 1926884084
		{ &Z_Construct_UFunction_UUMG_Item_Set_Menu_Ref, "Set_Menu_Ref" }, // 634012321
		{ &Z_Construct_UFunction_UUMG_Item_Set_Shop_Ref, "Set_Shop_Ref" }, // 2569166490
		{ &Z_Construct_UFunction_UUMG_Item_Spawn_Actors, "Spawn_Actors" }, // 4114783345
		{ &Z_Construct_UFunction_UUMG_Item_StartButtonClicked, "StartButtonClicked" }, // 4268237205
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "UMG_Item.h" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Button_Buy_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Button_Buy = { "Button_Buy", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Button_Buy), Z_Construct_UClass_UButton_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Button_Buy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Button_Buy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_OverlayC_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_OverlayC = { "OverlayC", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, OverlayC), Z_Construct_UClass_UOverlay_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_OverlayC_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_OverlayC_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Back_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Back = { "Image_Back", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Image_Back), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Back_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Back_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Mid_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Mid = { "Image_Mid", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Image_Mid), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Mid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Mid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_CanvasPanelC_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_CanvasPanelC = { "CanvasPanelC", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, CanvasPanelC), Z_Construct_UClass_UCanvasPanel_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_CanvasPanelC_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_CanvasPanelC_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_TextBlock_Description_MetaData[] = {
		{ "BindWidget", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_TextBlock_Description = { "TextBlock_Description", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, TextBlock_Description), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_TextBlock_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_TextBlock_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Shop_Ref_MetaData[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Shop_Ref = { "Shop_Ref", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Shop_Ref), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Shop_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Shop_Ref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Char_Ref_MetaData[] = {
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Char_Ref = { "Char_Ref", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Char_Ref), Z_Construct_UClass_AMT_TKCharacter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Char_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Char_Ref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Menu_Ref_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Menu_Ref = { "Menu_Ref", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Menu_Ref), Z_Construct_UClass_UMenu_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Menu_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Menu_Ref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUMG_Item_Statics::NewProp_Actor_To_Spawn_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "UMG_Item" },
		{ "ModuleRelativePath", "UMG_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UUMG_Item_Statics::NewProp_Actor_To_Spawn = { "Actor_To_Spawn", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUMG_Item, Actor_To_Spawn), Z_Construct_UClass_AItem_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Actor_To_Spawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::NewProp_Actor_To_Spawn_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUMG_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Button_Buy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_OverlayC,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Back,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Image_Mid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_CanvasPanelC,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_TextBlock_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Shop_Ref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Char_Ref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Menu_Ref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUMG_Item_Statics::NewProp_Actor_To_Spawn,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUMG_Item_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUMG_Item>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUMG_Item_Statics::ClassParams = {
		&UUMG_Item::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UUMG_Item_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUMG_Item_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUMG_Item_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUMG_Item()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUMG_Item_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUMG_Item, 928894064);
	template<> MT_TK_API UClass* StaticClass<UUMG_Item>()
	{
		return UUMG_Item::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUMG_Item(Z_Construct_UClass_UUMG_Item, &UUMG_Item::StaticClass, TEXT("/Script/MT_TK"), TEXT("UUMG_Item"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUMG_Item);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
