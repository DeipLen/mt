// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/MT_TKPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMT_TKPlayerController() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKPlayerController_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	DEFINE_FUNCTION(AMT_TKPlayerController::execNAM_3)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NAM_3();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKPlayerController::execNAM_2)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NAM_2();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKPlayerController::execNAM_1)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NAM_1();
		P_NATIVE_END;
	}
	void AMT_TKPlayerController::StaticRegisterNativesAMT_TKPlayerController()
	{
		UClass* Class = AMT_TKPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "NAM_1", &AMT_TKPlayerController::execNAM_1 },
			{ "NAM_2", &AMT_TKPlayerController::execNAM_2 },
			{ "NAM_3", &AMT_TKPlayerController::execNAM_3 },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMT_TKPlayerController_NAM_1_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKPlayerController_NAM_1_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKPlayerController_NAM_1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKPlayerController, nullptr, "NAM_1", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKPlayerController_NAM_1_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKPlayerController_NAM_1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKPlayerController_NAM_1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKPlayerController_NAM_1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKPlayerController_NAM_2_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKPlayerController_NAM_2_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKPlayerController_NAM_2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKPlayerController, nullptr, "NAM_2", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKPlayerController_NAM_2_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKPlayerController_NAM_2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKPlayerController_NAM_2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKPlayerController_NAM_2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKPlayerController_NAM_3_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKPlayerController_NAM_3_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKPlayerController_NAM_3_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKPlayerController, nullptr, "NAM_3", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKPlayerController_NAM_3_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKPlayerController_NAM_3_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKPlayerController_NAM_3()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKPlayerController_NAM_3_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMT_TKPlayerController_NoRegister()
	{
		return AMT_TKPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AMT_TKPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMT_TKPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMT_TKPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMT_TKPlayerController_NAM_1, "NAM_1" }, // 61746816
		{ &Z_Construct_UFunction_AMT_TKPlayerController_NAM_2, "NAM_2" }, // 1886940344
		{ &Z_Construct_UFunction_AMT_TKPlayerController_NAM_3, "NAM_3" }, // 3270373071
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MT_TKPlayerController.h" },
		{ "ModuleRelativePath", "MT_TKPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMT_TKPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMT_TKPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMT_TKPlayerController_Statics::ClassParams = {
		&AMT_TKPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMT_TKPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMT_TKPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMT_TKPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMT_TKPlayerController, 4208432356);
	template<> MT_TK_API UClass* StaticClass<AMT_TKPlayerController>()
	{
		return AMT_TKPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMT_TKPlayerController(Z_Construct_UClass_AMT_TKPlayerController, &AMT_TKPlayerController::StaticClass, TEXT("/Script/MT_TK"), TEXT("AMT_TKPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMT_TKPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
