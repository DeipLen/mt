// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_Use_Item_Three_generated_h
#error "Use_Item_Three.generated.h already included, missing '#pragma once' in Use_Item_Three.h"
#endif
#define MT_TK_Use_Item_Three_generated_h

#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_SPARSE_DATA
#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_RPC_WRAPPERS
#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUse_Item_Three(); \
	friend struct Z_Construct_UClass_AUse_Item_Three_Statics; \
public: \
	DECLARE_CLASS(AUse_Item_Three, AUse_Item, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AUse_Item_Three)


#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUse_Item_Three(); \
	friend struct Z_Construct_UClass_AUse_Item_Three_Statics; \
public: \
	DECLARE_CLASS(AUse_Item_Three, AUse_Item, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AUse_Item_Three)


#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUse_Item_Three(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUse_Item_Three) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUse_Item_Three); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUse_Item_Three); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUse_Item_Three(AUse_Item_Three&&); \
	NO_API AUse_Item_Three(const AUse_Item_Three&); \
public:


#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUse_Item_Three(AUse_Item_Three&&); \
	NO_API AUse_Item_Three(const AUse_Item_Three&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUse_Item_Three); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUse_Item_Three); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUse_Item_Three)


#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_Use_Item_Three_h_12_PROLOG
#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_INCLASS \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Use_Item_Three_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Use_Item_Three_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class AUse_Item_Three>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Use_Item_Three_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
