// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_Item_Struct_generated_h
#error "Item_Struct.generated.h already included, missing '#pragma once' in Item_Struct.h"
#endif
#define MT_TK_Item_Struct_generated_h

#define MT_TK_Source_MT_TK_Item_Struct_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FInventory_Item_Statics; \
	MT_TK_API static class UScriptStruct* StaticStruct();


template<> MT_TK_API UScriptStruct* StaticStruct<struct FInventory_Item>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Item_Struct_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
