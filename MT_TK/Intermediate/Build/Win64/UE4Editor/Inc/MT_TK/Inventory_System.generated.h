// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FInventory_Item;
#ifdef MT_TK_Inventory_System_generated_h
#error "Inventory_System.generated.h already included, missing '#pragma once' in Inventory_System.h"
#endif
#define MT_TK_Inventory_System_generated_h

#define MT_TK_Source_MT_TK_Inventory_System_h_14_SPARSE_DATA
#define MT_TK_Source_MT_TK_Inventory_System_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChange_Item); \
	DECLARE_FUNCTION(execCount_Item); \
	DECLARE_FUNCTION(execDelete_Item); \
	DECLARE_FUNCTION(execFind_Item); \
	DECLARE_FUNCTION(execAdd_Item);


#define MT_TK_Source_MT_TK_Inventory_System_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChange_Item); \
	DECLARE_FUNCTION(execCount_Item); \
	DECLARE_FUNCTION(execDelete_Item); \
	DECLARE_FUNCTION(execFind_Item); \
	DECLARE_FUNCTION(execAdd_Item);


#define MT_TK_Source_MT_TK_Inventory_System_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInventory_System(); \
	friend struct Z_Construct_UClass_UInventory_System_Statics; \
public: \
	DECLARE_CLASS(UInventory_System, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UInventory_System)


#define MT_TK_Source_MT_TK_Inventory_System_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUInventory_System(); \
	friend struct Z_Construct_UClass_UInventory_System_Statics; \
public: \
	DECLARE_CLASS(UInventory_System, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UInventory_System)


#define MT_TK_Source_MT_TK_Inventory_System_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInventory_System(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInventory_System) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInventory_System); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInventory_System); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInventory_System(UInventory_System&&); \
	NO_API UInventory_System(const UInventory_System&); \
public:


#define MT_TK_Source_MT_TK_Inventory_System_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInventory_System(UInventory_System&&); \
	NO_API UInventory_System(const UInventory_System&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInventory_System); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInventory_System); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UInventory_System)


#define MT_TK_Source_MT_TK_Inventory_System_h_14_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_Inventory_System_h_11_PROLOG
#define MT_TK_Source_MT_TK_Inventory_System_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Inventory_System_h_14_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Inventory_System_h_14_SPARSE_DATA \
	MT_TK_Source_MT_TK_Inventory_System_h_14_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Inventory_System_h_14_INCLASS \
	MT_TK_Source_MT_TK_Inventory_System_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Inventory_System_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Inventory_System_h_14_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Inventory_System_h_14_SPARSE_DATA \
	MT_TK_Source_MT_TK_Inventory_System_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Inventory_System_h_14_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Inventory_System_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class UInventory_System>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Inventory_System_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
