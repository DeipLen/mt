// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Use_Item.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUse_Item() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AUse_Item_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AUse_Item();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	DEFINE_FUNCTION(AUse_Item::execStart_Logic)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Start_Logic();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUse_Item::execDo_Something)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Do_Something();
		P_NATIVE_END;
	}
	void AUse_Item::StaticRegisterNativesAUse_Item()
	{
		UClass* Class = AUse_Item::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Do_Something", &AUse_Item::execDo_Something },
			{ "Start_Logic", &AUse_Item::execStart_Logic },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AUse_Item_Do_Something_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUse_Item_Do_Something_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Use_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUse_Item_Do_Something_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUse_Item, nullptr, "Do_Something", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUse_Item_Do_Something_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUse_Item_Do_Something_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUse_Item_Do_Something()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUse_Item_Do_Something_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUse_Item_Start_Logic_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUse_Item_Start_Logic_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Use_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUse_Item_Start_Logic_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUse_Item, nullptr, "Start_Logic", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUse_Item_Start_Logic_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUse_Item_Start_Logic_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUse_Item_Start_Logic()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUse_Item_Start_Logic_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AUse_Item_NoRegister()
	{
		return AUse_Item::StaticClass();
	}
	struct Z_Construct_UClass_AUse_Item_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUse_Item_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AUse_Item_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AUse_Item_Do_Something, "Do_Something" }, // 1868283298
		{ &Z_Construct_UFunction_AUse_Item_Start_Logic, "Start_Logic" }, // 619934770
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUse_Item_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Use_Item.h" },
		{ "ModuleRelativePath", "Use_Item.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUse_Item_Statics::NewProp_Count_MetaData[] = {
		{ "ModuleRelativePath", "Use_Item.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AUse_Item_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUse_Item, Count), METADATA_PARAMS(Z_Construct_UClass_AUse_Item_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUse_Item_Statics::NewProp_Count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AUse_Item_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUse_Item_Statics::NewProp_Count,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUse_Item_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUse_Item>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUse_Item_Statics::ClassParams = {
		&AUse_Item::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AUse_Item_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AUse_Item_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AUse_Item_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUse_Item_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUse_Item()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUse_Item_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUse_Item, 2870736574);
	template<> MT_TK_API UClass* StaticClass<AUse_Item>()
	{
		return AUse_Item::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUse_Item(Z_Construct_UClass_AUse_Item, &AUse_Item::StaticClass, TEXT("/Script/MT_TK"), TEXT("AUse_Item"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUse_Item);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
