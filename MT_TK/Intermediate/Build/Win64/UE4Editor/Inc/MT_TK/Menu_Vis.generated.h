// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_Menu_Vis_generated_h
#error "Menu_Vis.generated.h already included, missing '#pragma once' in Menu_Vis.h"
#endif
#define MT_TK_Menu_Vis_generated_h

#define MT_TK_Source_MT_TK_Menu_Vis_h_15_SPARSE_DATA
#define MT_TK_Source_MT_TK_Menu_Vis_h_15_RPC_WRAPPERS
#define MT_TK_Source_MT_TK_Menu_Vis_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MT_TK_Source_MT_TK_Menu_Vis_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMenu_Vis(); \
	friend struct Z_Construct_UClass_UMenu_Vis_Statics; \
public: \
	DECLARE_CLASS(UMenu_Vis, UMenu, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UMenu_Vis)


#define MT_TK_Source_MT_TK_Menu_Vis_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMenu_Vis(); \
	friend struct Z_Construct_UClass_UMenu_Vis_Statics; \
public: \
	DECLARE_CLASS(UMenu_Vis, UMenu, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UMenu_Vis)


#define MT_TK_Source_MT_TK_Menu_Vis_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMenu_Vis(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMenu_Vis) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMenu_Vis); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMenu_Vis); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMenu_Vis(UMenu_Vis&&); \
	NO_API UMenu_Vis(const UMenu_Vis&); \
public:


#define MT_TK_Source_MT_TK_Menu_Vis_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMenu_Vis(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMenu_Vis(UMenu_Vis&&); \
	NO_API UMenu_Vis(const UMenu_Vis&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMenu_Vis); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMenu_Vis); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMenu_Vis)


#define MT_TK_Source_MT_TK_Menu_Vis_h_15_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_Menu_Vis_h_12_PROLOG
#define MT_TK_Source_MT_TK_Menu_Vis_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_INCLASS \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Menu_Vis_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Menu_Vis_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class UMenu_Vis>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Menu_Vis_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
