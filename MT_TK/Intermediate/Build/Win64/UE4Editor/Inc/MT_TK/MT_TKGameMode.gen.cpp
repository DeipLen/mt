// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/MT_TKGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMT_TKGameMode() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKGameMode_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	void AMT_TKGameMode::StaticRegisterNativesAMT_TKGameMode()
	{
	}
	UClass* Z_Construct_UClass_AMT_TKGameMode_NoRegister()
	{
		return AMT_TKGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMT_TKGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMT_TKGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MT_TKGameMode.h" },
		{ "ModuleRelativePath", "MT_TKGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMT_TKGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMT_TKGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMT_TKGameMode_Statics::ClassParams = {
		&AMT_TKGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMT_TKGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMT_TKGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMT_TKGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMT_TKGameMode, 107854946);
	template<> MT_TK_API UClass* StaticClass<AMT_TKGameMode>()
	{
		return AMT_TKGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMT_TKGameMode(Z_Construct_UClass_AMT_TKGameMode, &AMT_TKGameMode::StaticClass, TEXT("/Script/MT_TK"), TEXT("AMT_TKGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMT_TKGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
