// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/MyBuff_Speed_Up.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyBuff_Speed_Up() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AMyBuff_Speed_Up_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AMyBuff_Speed_Up();
	MT_TK_API UClass* Z_Construct_UClass_ABuff_Sys();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	void AMyBuff_Speed_Up::StaticRegisterNativesAMyBuff_Speed_Up()
	{
	}
	UClass* Z_Construct_UClass_AMyBuff_Speed_Up_NoRegister()
	{
		return AMyBuff_Speed_Up::StaticClass();
	}
	struct Z_Construct_UClass_AMyBuff_Speed_Up_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyBuff_Speed_Up_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABuff_Sys,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyBuff_Speed_Up_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyBuff_Speed_Up.h" },
		{ "ModuleRelativePath", "MyBuff_Speed_Up.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyBuff_Speed_Up_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyBuff_Speed_Up>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyBuff_Speed_Up_Statics::ClassParams = {
		&AMyBuff_Speed_Up::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyBuff_Speed_Up_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyBuff_Speed_Up_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyBuff_Speed_Up()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyBuff_Speed_Up_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyBuff_Speed_Up, 2921840109);
	template<> MT_TK_API UClass* StaticClass<AMyBuff_Speed_Up>()
	{
		return AMyBuff_Speed_Up::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyBuff_Speed_Up(Z_Construct_UClass_AMyBuff_Speed_Up, &AMyBuff_Speed_Up::StaticClass, TEXT("/Script/MT_TK"), TEXT("AMyBuff_Speed_Up"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyBuff_Speed_Up);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
