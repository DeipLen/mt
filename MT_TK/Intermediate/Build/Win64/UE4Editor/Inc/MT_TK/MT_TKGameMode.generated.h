// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_MT_TKGameMode_generated_h
#error "MT_TKGameMode.generated.h already included, missing '#pragma once' in MT_TKGameMode.h"
#endif
#define MT_TK_MT_TKGameMode_generated_h

#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_SPARSE_DATA
#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_RPC_WRAPPERS
#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMT_TKGameMode(); \
	friend struct Z_Construct_UClass_AMT_TKGameMode_Statics; \
public: \
	DECLARE_CLASS(AMT_TKGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), MT_TK_API) \
	DECLARE_SERIALIZER(AMT_TKGameMode)


#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMT_TKGameMode(); \
	friend struct Z_Construct_UClass_AMT_TKGameMode_Statics; \
public: \
	DECLARE_CLASS(AMT_TKGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), MT_TK_API) \
	DECLARE_SERIALIZER(AMT_TKGameMode)


#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MT_TK_API AMT_TKGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMT_TKGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MT_TK_API, AMT_TKGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMT_TKGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MT_TK_API AMT_TKGameMode(AMT_TKGameMode&&); \
	MT_TK_API AMT_TKGameMode(const AMT_TKGameMode&); \
public:


#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MT_TK_API AMT_TKGameMode(AMT_TKGameMode&&); \
	MT_TK_API AMT_TKGameMode(const AMT_TKGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MT_TK_API, AMT_TKGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMT_TKGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMT_TKGameMode)


#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_MT_TKGameMode_h_9_PROLOG
#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_SPARSE_DATA \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_INCLASS \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_MT_TKGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_SPARSE_DATA \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_MT_TKGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class AMT_TKGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_MT_TKGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
