// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AShop;
class AMT_TKCharacter;
class UWrapBox;
class UScrollBox;
class UOverlay;
class UImage;
class UButton;
#ifdef MT_TK_Menu_generated_h
#error "Menu.generated.h already included, missing '#pragma once' in Menu.h"
#endif
#define MT_TK_Menu_generated_h

#define MT_TK_Source_MT_TK_Menu_h_19_SPARSE_DATA
#define MT_TK_Source_MT_TK_Menu_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_Char_Ref); \
	DECLARE_FUNCTION(execGet_WrapBoxC); \
	DECLARE_FUNCTION(execGet_ScrollBoxC); \
	DECLARE_FUNCTION(execGet_OverlayC); \
	DECLARE_FUNCTION(execGet_Image_Mid); \
	DECLARE_FUNCTION(execGet_Image_Back); \
	DECLARE_FUNCTION(execGet_Button_Exit); \
	DECLARE_FUNCTION(execSet_Shop_Ref); \
	DECLARE_FUNCTION(execSet_Char_Ref); \
	DECLARE_FUNCTION(execStartButtonClicked); \
	DECLARE_FUNCTION(execAddUMGItem);


#define MT_TK_Source_MT_TK_Menu_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_Char_Ref); \
	DECLARE_FUNCTION(execGet_WrapBoxC); \
	DECLARE_FUNCTION(execGet_ScrollBoxC); \
	DECLARE_FUNCTION(execGet_OverlayC); \
	DECLARE_FUNCTION(execGet_Image_Mid); \
	DECLARE_FUNCTION(execGet_Image_Back); \
	DECLARE_FUNCTION(execGet_Button_Exit); \
	DECLARE_FUNCTION(execSet_Shop_Ref); \
	DECLARE_FUNCTION(execSet_Char_Ref); \
	DECLARE_FUNCTION(execStartButtonClicked); \
	DECLARE_FUNCTION(execAddUMGItem);


#define MT_TK_Source_MT_TK_Menu_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMenu(); \
	friend struct Z_Construct_UClass_UMenu_Statics; \
public: \
	DECLARE_CLASS(UMenu, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UMenu)


#define MT_TK_Source_MT_TK_Menu_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUMenu(); \
	friend struct Z_Construct_UClass_UMenu_Statics; \
public: \
	DECLARE_CLASS(UMenu, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(UMenu)


#define MT_TK_Source_MT_TK_Menu_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMenu(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMenu) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMenu); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMenu); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMenu(UMenu&&); \
	NO_API UMenu(const UMenu&); \
public:


#define MT_TK_Source_MT_TK_Menu_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMenu(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMenu(UMenu&&); \
	NO_API UMenu(const UMenu&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMenu); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMenu); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMenu)


#define MT_TK_Source_MT_TK_Menu_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Button_Exit() { return STRUCT_OFFSET(UMenu, Button_Exit); } \
	FORCEINLINE static uint32 __PPO__Image_Back() { return STRUCT_OFFSET(UMenu, Image_Back); } \
	FORCEINLINE static uint32 __PPO__Image_Mid() { return STRUCT_OFFSET(UMenu, Image_Mid); } \
	FORCEINLINE static uint32 __PPO__OverlayC() { return STRUCT_OFFSET(UMenu, OverlayC); } \
	FORCEINLINE static uint32 __PPO__ScrollBoxC() { return STRUCT_OFFSET(UMenu, ScrollBoxC); } \
	FORCEINLINE static uint32 __PPO__WrapBoxC() { return STRUCT_OFFSET(UMenu, WrapBoxC); } \
	FORCEINLINE static uint32 __PPO__Char_Ref() { return STRUCT_OFFSET(UMenu, Char_Ref); } \
	FORCEINLINE static uint32 __PPO__Shop_Ref() { return STRUCT_OFFSET(UMenu, Shop_Ref); }


#define MT_TK_Source_MT_TK_Menu_h_16_PROLOG
#define MT_TK_Source_MT_TK_Menu_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Menu_h_19_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Menu_h_19_SPARSE_DATA \
	MT_TK_Source_MT_TK_Menu_h_19_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Menu_h_19_INCLASS \
	MT_TK_Source_MT_TK_Menu_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Menu_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Menu_h_19_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Menu_h_19_SPARSE_DATA \
	MT_TK_Source_MT_TK_Menu_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Menu_h_19_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Menu_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class UMenu>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Menu_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
