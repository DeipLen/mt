// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMenu;
#ifdef MT_TK_Shop_generated_h
#error "Shop.generated.h already included, missing '#pragma once' in Shop.h"
#endif
#define MT_TK_Shop_generated_h

#define MT_TK_Source_MT_TK_Shop_h_15_SPARSE_DATA
#define MT_TK_Source_MT_TK_Shop_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGet_Menu_Ref); \
	DECLARE_FUNCTION(execGet_Is_Start); \
	DECLARE_FUNCTION(execSet_Menu_Ref); \
	DECLARE_FUNCTION(execSet_Is_Start);


#define MT_TK_Source_MT_TK_Shop_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGet_Menu_Ref); \
	DECLARE_FUNCTION(execGet_Is_Start); \
	DECLARE_FUNCTION(execSet_Menu_Ref); \
	DECLARE_FUNCTION(execSet_Is_Start);


#define MT_TK_Source_MT_TK_Shop_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShop(); \
	friend struct Z_Construct_UClass_AShop_Statics; \
public: \
	DECLARE_CLASS(AShop, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AShop) \
	virtual UObject* _getUObject() const override { return const_cast<AShop*>(this); }


#define MT_TK_Source_MT_TK_Shop_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAShop(); \
	friend struct Z_Construct_UClass_AShop_Statics; \
public: \
	DECLARE_CLASS(AShop, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AShop) \
	virtual UObject* _getUObject() const override { return const_cast<AShop*>(this); }


#define MT_TK_Source_MT_TK_Shop_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShop(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShop) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShop); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShop); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShop(AShop&&); \
	NO_API AShop(const AShop&); \
public:


#define MT_TK_Source_MT_TK_Shop_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShop(AShop&&); \
	NO_API AShop(const AShop&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShop); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShop); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShop)


#define MT_TK_Source_MT_TK_Shop_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Shop_Mesh() { return STRUCT_OFFSET(AShop, Shop_Mesh); } \
	FORCEINLINE static uint32 __PPO__Shop_Component() { return STRUCT_OFFSET(AShop, Shop_Component); } \
	FORCEINLINE static uint32 __PPO__Is_Start() { return STRUCT_OFFSET(AShop, Is_Start); } \
	FORCEINLINE static uint32 __PPO__Menu_Ref() { return STRUCT_OFFSET(AShop, Menu_Ref); }


#define MT_TK_Source_MT_TK_Shop_h_12_PROLOG
#define MT_TK_Source_MT_TK_Shop_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Shop_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Shop_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Shop_h_15_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Shop_h_15_INCLASS \
	MT_TK_Source_MT_TK_Shop_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Shop_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Shop_h_15_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Shop_h_15_SPARSE_DATA \
	MT_TK_Source_MT_TK_Shop_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Shop_h_15_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Shop_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class AShop>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Shop_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
