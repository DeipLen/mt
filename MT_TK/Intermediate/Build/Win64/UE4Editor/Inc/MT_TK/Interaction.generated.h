// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef MT_TK_Interaction_generated_h
#error "Interaction.generated.h already included, missing '#pragma once' in Interaction.h"
#endif
#define MT_TK_Interaction_generated_h

#define MT_TK_Source_MT_TK_Interaction_h_13_SPARSE_DATA
#define MT_TK_Source_MT_TK_Interaction_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInteract);


#define MT_TK_Source_MT_TK_Interaction_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInteract);


#define MT_TK_Source_MT_TK_Interaction_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MT_TK_API UInteraction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteraction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MT_TK_API, UInteraction); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteraction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MT_TK_API UInteraction(UInteraction&&); \
	MT_TK_API UInteraction(const UInteraction&); \
public:


#define MT_TK_Source_MT_TK_Interaction_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MT_TK_API UInteraction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MT_TK_API UInteraction(UInteraction&&); \
	MT_TK_API UInteraction(const UInteraction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MT_TK_API, UInteraction); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteraction); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteraction)


#define MT_TK_Source_MT_TK_Interaction_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteraction(); \
	friend struct Z_Construct_UClass_UInteraction_Statics; \
public: \
	DECLARE_CLASS(UInteraction, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/MT_TK"), MT_TK_API) \
	DECLARE_SERIALIZER(UInteraction)


#define MT_TK_Source_MT_TK_Interaction_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	MT_TK_Source_MT_TK_Interaction_h_13_GENERATED_UINTERFACE_BODY() \
	MT_TK_Source_MT_TK_Interaction_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Interaction_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	MT_TK_Source_MT_TK_Interaction_h_13_GENERATED_UINTERFACE_BODY() \
	MT_TK_Source_MT_TK_Interaction_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Interaction_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteraction() {} \
public: \
	typedef UInteraction UClassType; \
	typedef IInteraction ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define MT_TK_Source_MT_TK_Interaction_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteraction() {} \
public: \
	typedef UInteraction UClassType; \
	typedef IInteraction ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define MT_TK_Source_MT_TK_Interaction_h_10_PROLOG
#define MT_TK_Source_MT_TK_Interaction_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Interaction_h_13_SPARSE_DATA \
	MT_TK_Source_MT_TK_Interaction_h_13_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Interaction_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Interaction_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Interaction_h_13_SPARSE_DATA \
	MT_TK_Source_MT_TK_Interaction_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Interaction_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class UInteraction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Interaction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
