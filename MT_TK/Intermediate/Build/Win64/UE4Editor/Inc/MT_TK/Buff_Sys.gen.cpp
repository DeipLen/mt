// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/Buff_Sys.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuff_Sys() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_ABuff_Sys_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_ABuff_Sys();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
// End Cross Module References
	DEFINE_FUNCTION(ABuff_Sys::execOut)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Out();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABuff_Sys::execIn)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->In();
		P_NATIVE_END;
	}
	void ABuff_Sys::StaticRegisterNativesABuff_Sys()
	{
		UClass* Class = ABuff_Sys::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "In", &ABuff_Sys::execIn },
			{ "Out", &ABuff_Sys::execOut },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABuff_Sys_In_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuff_Sys_In_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff_Sys.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuff_Sys_In_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuff_Sys, nullptr, "In", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuff_Sys_In_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuff_Sys_In_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuff_Sys_In()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuff_Sys_In_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABuff_Sys_Out_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuff_Sys_Out_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff_Sys.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuff_Sys_Out_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuff_Sys, nullptr, "Out", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuff_Sys_Out_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuff_Sys_Out_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuff_Sys_Out()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuff_Sys_Out_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABuff_Sys_NoRegister()
	{
		return ABuff_Sys::StaticClass();
	}
	struct Z_Construct_UClass_ABuff_Sys_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABuff_Sys_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABuff_Sys_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABuff_Sys_In, "In" }, // 2598033377
		{ &Z_Construct_UFunction_ABuff_Sys_Out, "Out" }, // 2045303310
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuff_Sys_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Buff_Sys.h" },
		{ "ModuleRelativePath", "Buff_Sys.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABuff_Sys_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABuff_Sys>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABuff_Sys_Statics::ClassParams = {
		&ABuff_Sys::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABuff_Sys_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABuff_Sys_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABuff_Sys()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABuff_Sys_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABuff_Sys, 1083132814);
	template<> MT_TK_API UClass* StaticClass<ABuff_Sys>()
	{
		return ABuff_Sys::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABuff_Sys(Z_Construct_UClass_ABuff_Sys, &ABuff_Sys::StaticClass, TEXT("/Script/MT_TK"), TEXT("ABuff_Sys"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABuff_Sys);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
