// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MT_TK_Buff_Sys_generated_h
#error "Buff_Sys.generated.h already included, missing '#pragma once' in Buff_Sys.h"
#endif
#define MT_TK_Buff_Sys_generated_h

#define MT_TK_Source_MT_TK_Buff_Sys_h_13_SPARSE_DATA
#define MT_TK_Source_MT_TK_Buff_Sys_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOut); \
	DECLARE_FUNCTION(execIn);


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOut); \
	DECLARE_FUNCTION(execIn);


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABuff_Sys(); \
	friend struct Z_Construct_UClass_ABuff_Sys_Statics; \
public: \
	DECLARE_CLASS(ABuff_Sys, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(ABuff_Sys)


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABuff_Sys(); \
	friend struct Z_Construct_UClass_ABuff_Sys_Statics; \
public: \
	DECLARE_CLASS(ABuff_Sys, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(ABuff_Sys)


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuff_Sys(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuff_Sys) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuff_Sys); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuff_Sys); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuff_Sys(ABuff_Sys&&); \
	NO_API ABuff_Sys(const ABuff_Sys&); \
public:


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuff_Sys(ABuff_Sys&&); \
	NO_API ABuff_Sys(const ABuff_Sys&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuff_Sys); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuff_Sys); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABuff_Sys)


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_PRIVATE_PROPERTY_OFFSET
#define MT_TK_Source_MT_TK_Buff_Sys_h_10_PROLOG
#define MT_TK_Source_MT_TK_Buff_Sys_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_SPARSE_DATA \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_INCLASS \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_Buff_Sys_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_SPARSE_DATA \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_Buff_Sys_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class ABuff_Sys>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_Buff_Sys_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
