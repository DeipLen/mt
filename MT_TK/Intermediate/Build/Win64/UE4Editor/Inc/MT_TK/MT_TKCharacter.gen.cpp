// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MT_TK/MT_TKCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMT_TKCharacter() {}
// Cross Module References
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKCharacter_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_AMT_TKCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_MT_TK();
	MT_TK_API UClass* Z_Construct_UClass_AShop_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCapsuleComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDecalComponent_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UInventory_System_NoRegister();
	MT_TK_API UClass* Z_Construct_UClass_UInteraction_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AMT_TKCharacter::execSet_Shop_Ref)
	{
		P_GET_OBJECT(AShop,Z_Param_A);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Shop_Ref(Z_Param_A);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execGet_Shop_Ref)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AShop**)Z_Param__Result=P_THIS->Get_Shop_Ref();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execGet_Was_Focus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->Get_Was_Focus();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execGet_Money)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->Get_Money();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execGet_Menu_Is_Open)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->Get_Menu_Is_Open();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execSet_Was_Focus)
	{
		P_GET_UBOOL(Z_Param_B);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Was_Focus(Z_Param_B);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execSet_Money)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_I);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Money(Z_Param_I);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execUse)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Filter);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Use(Z_Param_Filter);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execSet_Menu_Is_Open)
	{
		P_GET_UBOOL(Z_Param_B);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Set_Menu_Is_Open(Z_Param_B);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMT_TKCharacter::execOnOverlap)
	{
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnOverlap(Z_Param_OverlappedActor,Z_Param_OtherActor);
		P_NATIVE_END;
	}
	void AMT_TKCharacter::StaticRegisterNativesAMT_TKCharacter()
	{
		UClass* Class = AMT_TKCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Get_Menu_Is_Open", &AMT_TKCharacter::execGet_Menu_Is_Open },
			{ "Get_Money", &AMT_TKCharacter::execGet_Money },
			{ "Get_Shop_Ref", &AMT_TKCharacter::execGet_Shop_Ref },
			{ "Get_Was_Focus", &AMT_TKCharacter::execGet_Was_Focus },
			{ "OnOverlap", &AMT_TKCharacter::execOnOverlap },
			{ "Set_Menu_Is_Open", &AMT_TKCharacter::execSet_Menu_Is_Open },
			{ "Set_Money", &AMT_TKCharacter::execSet_Money },
			{ "Set_Shop_Ref", &AMT_TKCharacter::execSet_Shop_Ref },
			{ "Set_Was_Focus", &AMT_TKCharacter::execSet_Was_Focus },
			{ "Use", &AMT_TKCharacter::execUse },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics
	{
		struct MT_TKCharacter_eventGet_Menu_Is_Open_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MT_TKCharacter_eventGet_Menu_Is_Open_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MT_TKCharacter_eventGet_Menu_Is_Open_Parms), &Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Get_Menu_Is_Open", nullptr, nullptr, sizeof(MT_TKCharacter_eventGet_Menu_Is_Open_Parms), Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics
	{
		struct MT_TKCharacter_eventGet_Money_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventGet_Money_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Get_Money", nullptr, nullptr, sizeof(MT_TKCharacter_eventGet_Money_Parms), Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Get_Money()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Get_Money_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics
	{
		struct MT_TKCharacter_eventGet_Shop_Ref_Parms
		{
			AShop* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventGet_Shop_Ref_Parms, ReturnValue), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Get_Shop_Ref", nullptr, nullptr, sizeof(MT_TKCharacter_eventGet_Shop_Ref_Parms), Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics
	{
		struct MT_TKCharacter_eventGet_Was_Focus_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MT_TKCharacter_eventGet_Was_Focus_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MT_TKCharacter_eventGet_Was_Focus_Parms), &Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Get_Was_Focus", nullptr, nullptr, sizeof(MT_TKCharacter_eventGet_Was_Focus_Parms), Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics
	{
		struct MT_TKCharacter_eventOnOverlap_Parms
		{
			AActor* OverlappedActor;
			AActor* OtherActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::NewProp_OverlappedActor = { "OverlappedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventOnOverlap_Parms, OverlappedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventOnOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::NewProp_OverlappedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//??????? ??????????????\n" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "OnOverlap", nullptr, nullptr, sizeof(MT_TKCharacter_eventOnOverlap_Parms), Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_OnOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_OnOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics
	{
		struct MT_TKCharacter_eventSet_Menu_Is_Open_Parms
		{
			bool B;
		};
		static void NewProp_B_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_B;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::NewProp_B_SetBit(void* Obj)
	{
		((MT_TKCharacter_eventSet_Menu_Is_Open_Parms*)Obj)->B = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MT_TKCharacter_eventSet_Menu_Is_Open_Parms), &Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::NewProp_B_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::NewProp_B,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//?????????????\n" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Set_Menu_Is_Open", nullptr, nullptr, sizeof(MT_TKCharacter_eventSet_Menu_Is_Open_Parms), Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics
	{
		struct MT_TKCharacter_eventSet_Money_Parms
		{
			int32 I;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_I;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::NewProp_I = { "I", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventSet_Money_Parms, I), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::NewProp_I,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Set_Money", nullptr, nullptr, sizeof(MT_TKCharacter_eventSet_Money_Parms), Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Set_Money()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Set_Money_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics
	{
		struct MT_TKCharacter_eventSet_Shop_Ref_Parms
		{
			AShop* A;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_A;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventSet_Shop_Ref_Parms, A), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::NewProp_A,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Set_Shop_Ref", nullptr, nullptr, sizeof(MT_TKCharacter_eventSet_Shop_Ref_Parms), Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics
	{
		struct MT_TKCharacter_eventSet_Was_Focus_Parms
		{
			bool B;
		};
		static void NewProp_B_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_B;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::NewProp_B_SetBit(void* Obj)
	{
		((MT_TKCharacter_eventSet_Was_Focus_Parms*)Obj)->B = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MT_TKCharacter_eventSet_Was_Focus_Parms), &Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::NewProp_B_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::NewProp_B,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Set_Was_Focus", nullptr, nullptr, sizeof(MT_TKCharacter_eventSet_Was_Focus_Parms), Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMT_TKCharacter_Use_Statics
	{
		struct MT_TKCharacter_eventUse_Parms
		{
			FString Filter;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Filter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MT_TKCharacter_eventUse_Parms, Filter), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::NewProp_Filter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMT_TKCharacter, nullptr, "Use", nullptr, nullptr, sizeof(MT_TKCharacter_eventUse_Parms), Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMT_TKCharacter_Use()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMT_TKCharacter_Use_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMT_TKCharacter_NoRegister()
	{
		return AMT_TKCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AMT_TKCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Capssule_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Capssule;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TopDownCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TopDownCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CursorToWorld_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CursorToWorld;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Menu_Is_Open_MetaData[];
#endif
		static void NewProp_Menu_Is_Open_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Menu_Is_Open;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Money_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Money;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Was_Focus_MetaData[];
#endif
		static void NewProp_Was_Focus_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Was_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shop_Ref_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Shop_Ref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Items_System_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Items_System;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMT_TKCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_MT_TK,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMT_TKCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMT_TKCharacter_Get_Menu_Is_Open, "Get_Menu_Is_Open" }, // 3197812010
		{ &Z_Construct_UFunction_AMT_TKCharacter_Get_Money, "Get_Money" }, // 2308509065
		{ &Z_Construct_UFunction_AMT_TKCharacter_Get_Shop_Ref, "Get_Shop_Ref" }, // 1027850642
		{ &Z_Construct_UFunction_AMT_TKCharacter_Get_Was_Focus, "Get_Was_Focus" }, // 880281600
		{ &Z_Construct_UFunction_AMT_TKCharacter_OnOverlap, "OnOverlap" }, // 736564935
		{ &Z_Construct_UFunction_AMT_TKCharacter_Set_Menu_Is_Open, "Set_Menu_Is_Open" }, // 3287988243
		{ &Z_Construct_UFunction_AMT_TKCharacter_Set_Money, "Set_Money" }, // 857530553
		{ &Z_Construct_UFunction_AMT_TKCharacter_Set_Shop_Ref, "Set_Shop_Ref" }, // 2324049668
		{ &Z_Construct_UFunction_AMT_TKCharacter_Set_Was_Focus, "Set_Was_Focus" }, // 979168886
		{ &Z_Construct_UFunction_AMT_TKCharacter_Use, "Use" }, // 4148334492
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "MT_TKCharacter.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Capssule_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Capssule = { "Capssule", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, Capssule), Z_Construct_UClass_UCapsuleComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Capssule_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Capssule_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_TopDownCameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Top down camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
		{ "ToolTip", "Top down camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_TopDownCameraComponent = { "TopDownCameraComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, TopDownCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_TopDownCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_TopDownCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera above the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera above the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CursorToWorld_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** A decal that projects to the cursor location. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
		{ "ToolTip", "A decal that projects to the cursor location." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CursorToWorld = { "CursorToWorld", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, CursorToWorld), Z_Construct_UClass_UDecalComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CursorToWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CursorToWorld_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open_MetaData[] = {
		{ "Comment", "//????????? ????\n" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open_SetBit(void* Obj)
	{
		((AMT_TKCharacter*)Obj)->Menu_Is_Open = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open = { "Menu_Is_Open", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMT_TKCharacter), &Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Money_MetaData[] = {
		{ "Comment", "//??????? ???????\n" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Money = { "Money", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, Money), METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Money_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Money_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus_MetaData[] = {
		{ "Comment", "//??????? ?????? ?? ????????\n" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus_SetBit(void* Obj)
	{
		((AMT_TKCharacter*)Obj)->Was_Focus = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus = { "Was_Focus", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMT_TKCharacter), &Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Shop_Ref_MetaData[] = {
		{ "Comment", "//?????? ?? ???????\n" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Shop_Ref = { "Shop_Ref", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, Shop_Ref), Z_Construct_UClass_AShop_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Shop_Ref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Shop_Ref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Items_System_MetaData[] = {
		{ "Comment", "//????????? ?????\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MT_TKCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Items_System = { "Items_System", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMT_TKCharacter, Items_System), Z_Construct_UClass_UInventory_System_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Items_System_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Items_System_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMT_TKCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Capssule,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_TopDownCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CameraBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_CursorToWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Menu_Is_Open,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Money,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Was_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Shop_Ref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMT_TKCharacter_Statics::NewProp_Items_System,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AMT_TKCharacter_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteraction_NoRegister, (int32)VTABLE_OFFSET(AMT_TKCharacter, IInteraction), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMT_TKCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMT_TKCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMT_TKCharacter_Statics::ClassParams = {
		&AMT_TKCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMT_TKCharacter_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMT_TKCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMT_TKCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMT_TKCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMT_TKCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMT_TKCharacter, 494200156);
	template<> MT_TK_API UClass* StaticClass<AMT_TKCharacter>()
	{
		return AMT_TKCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMT_TKCharacter(Z_Construct_UClass_AMT_TKCharacter, &AMT_TKCharacter::StaticClass, TEXT("/Script/MT_TK"), TEXT("AMT_TKCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMT_TKCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
