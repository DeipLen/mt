// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AShop;
class AActor;
#ifdef MT_TK_MT_TKCharacter_generated_h
#error "MT_TKCharacter.generated.h already included, missing '#pragma once' in MT_TKCharacter.h"
#endif
#define MT_TK_MT_TKCharacter_generated_h

#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_SPARSE_DATA
#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_Was_Focus); \
	DECLARE_FUNCTION(execGet_Money); \
	DECLARE_FUNCTION(execGet_Menu_Is_Open); \
	DECLARE_FUNCTION(execSet_Was_Focus); \
	DECLARE_FUNCTION(execSet_Money); \
	DECLARE_FUNCTION(execUse); \
	DECLARE_FUNCTION(execSet_Menu_Is_Open); \
	DECLARE_FUNCTION(execOnOverlap);


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_Shop_Ref); \
	DECLARE_FUNCTION(execGet_Was_Focus); \
	DECLARE_FUNCTION(execGet_Money); \
	DECLARE_FUNCTION(execGet_Menu_Is_Open); \
	DECLARE_FUNCTION(execSet_Was_Focus); \
	DECLARE_FUNCTION(execSet_Money); \
	DECLARE_FUNCTION(execUse); \
	DECLARE_FUNCTION(execSet_Menu_Is_Open); \
	DECLARE_FUNCTION(execOnOverlap);


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMT_TKCharacter(); \
	friend struct Z_Construct_UClass_AMT_TKCharacter_Statics; \
public: \
	DECLARE_CLASS(AMT_TKCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AMT_TKCharacter) \
	virtual UObject* _getUObject() const override { return const_cast<AMT_TKCharacter*>(this); }


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAMT_TKCharacter(); \
	friend struct Z_Construct_UClass_AMT_TKCharacter_Statics; \
public: \
	DECLARE_CLASS(AMT_TKCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MT_TK"), NO_API) \
	DECLARE_SERIALIZER(AMT_TKCharacter) \
	virtual UObject* _getUObject() const override { return const_cast<AMT_TKCharacter*>(this); }


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMT_TKCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMT_TKCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMT_TKCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMT_TKCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMT_TKCharacter(AMT_TKCharacter&&); \
	NO_API AMT_TKCharacter(const AMT_TKCharacter&); \
public:


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMT_TKCharacter(AMT_TKCharacter&&); \
	NO_API AMT_TKCharacter(const AMT_TKCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMT_TKCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMT_TKCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMT_TKCharacter)


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Capssule() { return STRUCT_OFFSET(AMT_TKCharacter, Capssule); } \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(AMT_TKCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMT_TKCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(AMT_TKCharacter, CursorToWorld); } \
	FORCEINLINE static uint32 __PPO__Menu_Is_Open() { return STRUCT_OFFSET(AMT_TKCharacter, Menu_Is_Open); } \
	FORCEINLINE static uint32 __PPO__Money() { return STRUCT_OFFSET(AMT_TKCharacter, Money); } \
	FORCEINLINE static uint32 __PPO__Was_Focus() { return STRUCT_OFFSET(AMT_TKCharacter, Was_Focus); } \
	FORCEINLINE static uint32 __PPO__Shop_Ref() { return STRUCT_OFFSET(AMT_TKCharacter, Shop_Ref); }


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_16_PROLOG
#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_SPARSE_DATA \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_RPC_WRAPPERS \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_INCLASS \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MT_TK_Source_MT_TK_MT_TKCharacter_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_PRIVATE_PROPERTY_OFFSET \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_SPARSE_DATA \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_INCLASS_NO_PURE_DECLS \
	MT_TK_Source_MT_TK_MT_TKCharacter_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MT_TK_API UClass* StaticClass<class AMT_TKCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MT_TK_Source_MT_TK_MT_TKCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
