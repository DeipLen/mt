// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Item_Three.generated.h"

/**
 * 
 */
UCLASS()
class MT_TK_API AItem_Three : public AItem
{
	GENERATED_BODY()

public:
	AItem_Three();
};
