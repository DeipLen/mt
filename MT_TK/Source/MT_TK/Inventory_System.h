// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item_Struct.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Inventory_System.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MT_TK_API UInventory_System : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventory_System();


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	FInventory_Item Data;
	TArray<FInventory_Item> Inventory;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
		void Add_Item(FInventory_Item FI);

	UFUNCTION()
		FInventory_Item Find_Item(FInventory_Item FI);

	UFUNCTION()
		void Delete_Item(FInventory_Item FI);

	UFUNCTION()
		int Count_Item(FInventory_Item FI);

	UFUNCTION()
		void Change_Item(FInventory_Item FI);
};
