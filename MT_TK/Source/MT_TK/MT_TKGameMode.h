// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MT_TKGameMode.generated.h"

UCLASS(minimalapi)
class AMT_TKGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMT_TKGameMode();
};



