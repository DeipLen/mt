// Fill out your copyright notice in the Description page of Project Settings.


#include "UMG_Item.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/Overlay.h"
#include "Components/TextBlock.h"
#include "Components/CanvasPanel.h"

bool UUMG_Item::Initialize()
{
    Super::Initialize();

    Button_Buy->OnClicked.AddDynamic(this, &UUMG_Item::StartButtonClicked);
    Image_Back->SetColorAndOpacity(FLinearColor(0.298937, 0.302083, 0.29579, 0.485714));
    return false;
}

void UUMG_Item::Set_TextBlock_Description(FString Text)
{
    this->TextBlock_Description->SetText(FText::FromString(Text));
}

void UUMG_Item::StartButtonClicked()
{
    if (Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()) != nullptr)
    {
       if(Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->Get_Money() >= 10)
        {
           Char_Ref = Menu_Ref->Get_Char_Ref();
           Shop_Ref = Menu_Ref->Get_Shop_Ref();
           Spawn_Actors();
        }
    }
}

void UUMG_Item::Spawn_Actors() //Создание предметов
{
    FVector V;
    V.X = 0;
    V.Y = 70;
    V.Z = 100;
    FVector Location = (Shop_Ref->GetActorLocation() + (Shop_Ref->GetActorForwardVector() * 200)) + V.Z - V.Y;
    FRotator Rotation;
    Rotation.Yaw = 0;
    Rotation.Pitch = 0;
    Rotation.Roll = 0;
    FActorSpawnParameters SpawnParams;
    AItem* Item_Ref = GetWorld()->SpawnActor<AItem>(Actor_To_Spawn, Location, Rotation, SpawnParams);  

    Shop_Ref->Set_Is_Start(false);
    Char_Ref->Set_Menu_Is_Open(false);
    Menu_Ref->RemoveFromViewport();
}

void UUMG_Item::Set_Shop_Ref(AShop* AS)
{
    Shop_Ref = AS;
}

void UUMG_Item::Set_Char_Ref(AMT_TKCharacter* AM)
{
    Char_Ref = AM;
}

void UUMG_Item::Set_Menu_Ref(UMenu* UM)
{
    Menu_Ref = UM;
}

void UUMG_Item::Set_Actor_To_Spawn(TSubclassOf<AItem> TS)
{
    Actor_To_Spawn = TS;
}

UButton* UUMG_Item::Get_Button_Buy()
{
    return Button_Buy;
}

UOverlay* UUMG_Item::Get_OverlayC()
{
    return OverlayC;
}

UImage* UUMG_Item::Get_Image_Back()
{
    return Image_Back;
}

UImage* UUMG_Item::Get_Image_Mid()
{
    return Image_Mid;
}

UCanvasPanel* UUMG_Item::Get_CanvasPanelC()
{
    return CanvasPanelC;
}

UTextBlock* UUMG_Item::Get_TextBlock_Description()
{
    return TextBlock_Description;
}

AShop* UUMG_Item::Get_Shop_Ref()
{
    return Shop_Ref;
}

AMT_TKCharacter* UUMG_Item::Get_Char_Ref()
{
    return Char_Ref;
}

UMenu* UUMG_Item::Get_Menu_Ref()
{
    return Menu_Ref;
}

TSubclassOf<AItem> UUMG_Item::Get_Actor_To_Spawn()
{
    return Actor_To_Spawn;
}