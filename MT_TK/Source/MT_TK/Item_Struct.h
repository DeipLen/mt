// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item_Struct.generated.h"

USTRUCT(BlueprintType)
struct FInventory_Item
{
	GENERATED_BODY()

		UPROPERTY()
		FString Name;
	UPROPERTY()
	FString Use_Ref;
	UPROPERTY()
	int Count_Use;
};


