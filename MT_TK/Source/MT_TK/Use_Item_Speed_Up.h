// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Use_Item.h"
#include "Use_Item_Speed_Up.generated.h"

/**
 * 
 */
UCLASS()
class MT_TK_API AUse_Item_Speed_Up : public AUse_Item
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AUse_Item_Speed_Up();

	virtual void Start_Logic();

protected:

	virtual void Do_Something();

public:
	int32 CountdownTime;

	void AdvanceTimer();

	void CountdownHasFinished();

	FTimerHandle CountdownTimerHandle;

};
