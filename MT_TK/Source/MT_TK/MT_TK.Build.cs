// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MT_TK : ModuleRules
{
	public MT_TK(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
