// Fill out your copyright notice in the Description page of Project Settings.


#include "Use_Item_Size_Change.h"

AUse_Item_Size_Change::AUse_Item_Size_Change()
{

}

 void AUse_Item_Size_Change::Do_Something()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Work")));
	if (Count == 2)
	{
		if (Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()) != nullptr)
		{
			AShop* RF = Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->Get_Shop_Ref();
			RF->SetActorScale3D(RF->GetActorScale3D() * 2);
		}
	}
	else
	{
		if (Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()) != nullptr)
		{
			AShop* RF = Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->Get_Shop_Ref();
			RF->SetActorScale3D(RF->GetActorScale3D() / 2);
		}
	}
	
}

 void AUse_Item_Size_Change::Start_Logic()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Start"));
	Do_Something();
	Destroy();
}

