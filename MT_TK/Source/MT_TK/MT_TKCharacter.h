// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Item.h"
#include "Use_Item.h"
#include "Inventory_System.h"
#include "Shop.h"
#include "Coin.h"
#include "Interaction.h"
#include "Invetory_Component.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MT_TKCharacter.generated.h"

UCLASS(Blueprintable)
class AMT_TKCharacter : public ACharacter, public IInteraction
{
	GENERATED_BODY()

public:
	AMT_TKCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:

	UPROPERTY()
		class UCapsuleComponent* Capssule;
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;


	UPROPERTY() //��������� ����
		bool Menu_Is_Open;

	UPROPERTY() //������� �������
		int Money;

	UPROPERTY() //������� ������ �� ��������
		bool Was_Focus;

	UPROPERTY() //������ �� �������
		AShop* Shop_Ref;

public:
		virtual void Interact(AActor* Interactor) override; //������� ��������������

		UFUNCTION()
			void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);
			
		//�������������
		UFUNCTION()
			void Set_Menu_Is_Open(bool B);

		UFUNCTION()
			void Use(FString Filter);


		UFUNCTION()
			void Set_Money(int I);

		UFUNCTION()
			void Set_Was_Focus(bool B);

		UFUNCTION()
			bool Get_Menu_Is_Open();

		UFUNCTION()
			int Get_Money();

		UFUNCTION()
			bool Get_Was_Focus();

		UFUNCTION()
			AShop* Get_Shop_Ref();

		UFUNCTION()
			void Set_Shop_Ref(AShop* A);

		//��������� �����
		UPROPERTY()
			UInventory_System* Items_System;



		
};

