// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Shop.h"
#include "MT_TKCharacter.h"
#include "Menu.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UMG_Item.generated.h"

/**
 * 
 */
UCLASS() //����� ���� ���������
class MT_TK_API UUMG_Item : public UUserWidget
{
	GENERATED_BODY()
	
		virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Buy;

	UPROPERTY(meta = (BindWidget))
		class UOverlay* OverlayC;

	UPROPERTY(meta = (BindWidget))
		class UImage* Image_Back;

	UPROPERTY(meta = (BindWidget))
		class UImage* Image_Mid;

	UPROPERTY(meta = (BindWidget))
		class UCanvasPanel* CanvasPanelC;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TextBlock_Description;

	UPROPERTY()
		AShop* Shop_Ref;

	UPROPERTY()
		AMT_TKCharacter* Char_Ref;

	UPROPERTY()
		UMenu* Menu_Ref;

	UFUNCTION()
		void StartButtonClicked();

	UFUNCTION()
		void Spawn_Actors();

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AItem> Actor_To_Spawn;

public:

	void Set_TextBlock_Description(FString Text);

	UFUNCTION()
		void Set_Shop_Ref(AShop* AS);

	UFUNCTION()
		void Set_Char_Ref(AMT_TKCharacter* AM);

	UFUNCTION()
		void Set_Menu_Ref(UMenu* UM);

	UFUNCTION()
		void Set_Actor_To_Spawn(TSubclassOf<AItem> TS);

	UFUNCTION()
		UButton* Get_Button_Buy();

	UFUNCTION()
		UOverlay* Get_OverlayC();

	UFUNCTION()
		UImage* Get_Image_Back();

	UFUNCTION()
		UImage* Get_Image_Mid();

	UFUNCTION()
		UCanvasPanel* Get_CanvasPanelC();

	UFUNCTION()
		UTextBlock* Get_TextBlock_Description();

	UFUNCTION()
		AShop* Get_Shop_Ref();

	UFUNCTION()
		AMT_TKCharacter* Get_Char_Ref();

	UFUNCTION()
		UMenu* Get_Menu_Ref();

	UFUNCTION()
		TSubclassOf<AItem> Get_Actor_To_Spawn();

};
