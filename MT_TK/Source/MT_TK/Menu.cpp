// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/Overlay.h"
#include "Components/ScrollBox.h"
#include "Components/WrapBox.h"
#include "Components/TextBlock.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"


bool UMenu::Initialize()
{
    Super::Initialize();

    Button_Exit->OnClicked.AddDynamic(this, &UMenu::StartButtonClicked);
    Image_Back->SetColorAndOpacity(FLinearColor(0.009957, 0.010072, 0.010417, 0.771429));
    Image_Mid->SetColorAndOpacity(FLinearColor(0.054731, 0.054731, 0.057292, 0.904762));

	AddUMGItem("First", TEXT("'/Script/MT_TK.Item_Speed_Up'"));
    AddUMGItem("Second", TEXT("'/Script/MT_TK.Item_Three'"));
    AddUMGItem("Third", TEXT("'/Script/MT_TK.Item_Size_Change'"));

    return false;
}

void UMenu::StartButtonClicked()
{
	Char_Ref->Set_Menu_Is_Open(false);
	Shop_Ref->Set_Is_Start(false);
	this->RemoveFromViewport();
}

void UMenu::AddUMGItem(FString Text, FString Link) //���������� ���� ��������� � ����, � ����� �����������
{
	TSubclassOf<AItem> ATS;
	FStringClassReference Link_Try(Link);
	UClass* Link_Res = Link_Try.TryLoadClass<AItem>();
	if (Link_Res)
	{
		ATS = Link_Res;
	}

	FStringClassReference Text_Try(TEXT("WidgetBlueprint'/Game/TopDownCPP/Blueprints/BP_UMG_Item.BP_UMG_Item_C'"));
	UClass* Text_Res = Text_Try.TryLoadClass<UUMG_Item>();
	if (Text_Res)
	{
		UUMG_Item* UMG_Item_Ref = CreateWidget<UUMG_Item>(this->GetGameInstance(), Text_Res);
		if (UMG_Item_Ref)
		{
			UMG_Item_Ref->Set_TextBlock_Description(Text);
			UMG_Item_Ref->Set_Shop_Ref(Shop_Ref);
			UMG_Item_Ref->Set_Char_Ref(Char_Ref);
			UMG_Item_Ref->Set_Actor_To_Spawn(ATS);
			UMG_Item_Ref->Set_Menu_Ref(this);
			this->WrapBoxC->AddChild(UMG_Item_Ref);
		}
	}
}

void UMenu::Set_Char_Ref(AMT_TKCharacter* AM)
{
	Char_Ref = AM;
}

void UMenu::Set_Shop_Ref(AShop* AS)
{
	Shop_Ref = AS;
}

UButton* UMenu::Get_Button_Exit()
{
	return Button_Exit;
}

UImage* UMenu::Get_Image_Back()
{
	return Image_Back;
}

UImage* UMenu::Get_Image_Mid()
{
	return Image_Mid;
}

UOverlay* UMenu::Get_OverlayC()
{
	return OverlayC;
}

UScrollBox* UMenu::Get_ScrollBoxC()
{
	return ScrollBoxC;
}

UWrapBox* UMenu::Get_WrapBoxC()
{
	return WrapBoxC;
}

AMT_TKCharacter* UMenu::Get_Char_Ref()
{
	return Char_Ref;
}

AShop* UMenu::Get_Shop_Ref()
{
	return Shop_Ref;
}