// Fill out your copyright notice in the Description page of Project Settings.


#include "Item_Three.h"

AItem_Three::AItem_Three()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Meshes(TEXT("'/Game/TopDownCPP/Blueprints/Drop_Suzanne.Drop_Suzanne'"));

	if (Meshes.Succeeded())
	{
		Item_Mesh->SetStaticMesh(Meshes.Object);
		Item_Mesh->SetRelativeLocation(FVector(70.f, 0.f, 0.f));
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Mats(TEXT("Material'/Game/StarterContent/Materials/M_Metal_Rust.M_Metal_Rust'"));

	if (Mats.Succeeded())
	{
		Item_Mesh->SetMaterial(0, Mats.Object);
	}
	
	Data.Name = "Three";
	Data.Use_Ref = TEXT("Class'/Script/MT_TK.Use_Item_Three'");
	Data.Count_Use = 3;
}