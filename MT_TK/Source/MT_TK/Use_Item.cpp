// Fill out your copyright notice in the Description page of Project Settings.


#include "Use_Item.h"

// Sets default values
AUse_Item::AUse_Item()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Count = 0;

}

void AUse_Item::Do_Something()
{
}

void AUse_Item::Start_Logic()
{
	Do_Something();
	Destroy();
}
