// Copyright Epic Games, Inc. All Rights Reserved.

#include "MT_TK.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MT_TK, "MT_TK" );

DEFINE_LOG_CATEGORY(LogMT_TK)
 