// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Item_Size_Change.generated.h"

/**
 * 
 */
UCLASS()
class MT_TK_API AItem_Size_Change : public AItem
{
	GENERATED_BODY()

public:
	AItem_Size_Change();
};
