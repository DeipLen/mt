// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG_Item.h"
#include "Item_Struct.h"
#include "MT_TKCharacter.h"
#include "Interaction.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"



UCLASS() //����� ���������
class MT_TK_API AItem : public AActor, public IInteraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	FInventory_Item Data;


protected:

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* Item_Mesh;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Item_Component;

public:	
	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
		UStaticMeshComponent* Get_Item_Mesh();

	
};
