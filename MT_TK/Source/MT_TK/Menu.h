// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "Shop.h"
#include "MT_TKCharacter.h"
#include "UMG_Item.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Menu.generated.h"

/**
 * 
 */
UCLASS() //����� ����
class MT_TK_API UMenu : public UUserWidget
{
	GENERATED_BODY()

		virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Exit;

	UPROPERTY(meta = (BindWidget))
		class UImage* Image_Back;

	UPROPERTY(meta = (BindWidget))
		class UImage* Image_Mid;

	UPROPERTY(meta = (BindWidget))
		class UOverlay* OverlayC;

	UPROPERTY(meta = (BindWidget))
		class UScrollBox* ScrollBoxC;

	UPROPERTY(meta = (BindWidget))
		class UWrapBox* WrapBoxC;

	UPROPERTY()
		AMT_TKCharacter* Char_Ref;

	UPROPERTY()
		AShop* Shop_Ref;

	UFUNCTION()
		void AddUMGItem(FString Text, FString Link);

	UFUNCTION()
		void StartButtonClicked();

public:

	UFUNCTION()
		void Set_Char_Ref(AMT_TKCharacter* AM);

	UFUNCTION()
		void Set_Shop_Ref(AShop* AS);

	UFUNCTION()
		UButton* Get_Button_Exit();

	UFUNCTION()
		UImage* Get_Image_Back();

	UFUNCTION()
		UImage* Get_Image_Mid();

	UFUNCTION()
		UOverlay* Get_OverlayC();

	UFUNCTION()
		UScrollBox* Get_ScrollBoxC();

	UFUNCTION()
		UWrapBox* Get_WrapBoxC();

	UFUNCTION()
		AMT_TKCharacter* Get_Char_Ref();

	UFUNCTION()
		AShop* Get_Shop_Ref();
};
