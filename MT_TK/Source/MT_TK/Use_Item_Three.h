// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Use_Item.h"
#include "Use_Item_Three.generated.h"

/**
 * 
 */
UCLASS()
class MT_TK_API AUse_Item_Three : public AUse_Item
{
	GENERATED_BODY()

public:

	AUse_Item_Three();

	virtual void Start_Logic();

protected:
	
	virtual void Do_Something();
	
};
