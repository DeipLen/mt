// Copyright Epic Games, Inc. All Rights Reserved.

#include "MT_TKCharacter.h"
#include "Use_Item_Size_Change.h"
#include "Item_Struct.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"

AMT_TKCharacter::AMT_TKCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//�������������
	Menu_Is_Open = false;

	OnActorBeginOverlap.AddDynamic(this, &AMT_TKCharacter::OnOverlap);

	Money = 0;
	Was_Focus = false;

	Items_System = CreateDefaultSubobject<UInventory_System>(TEXT("Items_System"));


}

void AMT_TKCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (!Menu_Is_Open)
	{
		//Line Trace
		FHitResult Out_Hit;
		FCollisionQueryParams CollisionParams;
		FVector Start = GetCapsuleComponent()->GetComponentLocation();
		FVector End = Start + (GetCapsuleComponent()->GetForwardVector() * 400);

		DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

		bool Is_Hit = GetWorld()->LineTraceSingleByChannel(Out_Hit, Start, End, ECC_Visibility, CollisionParams);

		if (Out_Hit.bBlockingHit)
		{
			if (Cast<AShop>(Out_Hit.GetActor()) != nullptr)//�������������� � ���������
			{
				Set_Shop_Ref(Cast<AShop>(Out_Hit.GetActor()));
				Cast<AShop>(Out_Hit.GetActor())->Interact(this);
			}

			if (CursorToWorld != nullptr)
			{
				if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
				{
					if (UWorld* World = GetWorld())
					{
						FHitResult HitResult;
						FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
						FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
						FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
						Params.AddIgnoredActor(this);
						World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
						FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
						CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
					}
				}
				else if (APlayerController* PC = Cast<APlayerController>(GetController()))
				{
					FHitResult TraceHitResult;
					PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
					FVector CursorFV = TraceHitResult.ImpactNormal;
					FRotator CursorR = CursorFV.Rotation();
					CursorToWorld->SetWorldLocation(TraceHitResult.Location);
					CursorToWorld->SetWorldRotation(CursorR);
				}
			}
		}
		else
		{
			if (!Menu_Is_Open)
			{
				Was_Focus = false;
			}
		}
	}
}

void AMT_TKCharacter::Interact(AActor* Interactor)
{
}

void AMT_TKCharacter::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (Cast<IInteraction>(OtherActor) != nullptr)
	{
		Cast<IInteraction>(OtherActor)->Interact(OverlappedActor);
	}
}


void AMT_TKCharacter::Set_Menu_Is_Open(bool B)
{
	Menu_Is_Open = B;
}

void AMT_TKCharacter::Use(FString Filter) //��������������� � ����������
{
	FInventory_Item FI;
	FI.Name = Filter;

	if (Items_System->Find_Item(FI).Use_Ref != "")
	{
		
		FInventory_Item Item_Ref = Items_System->Find_Item(FI);
		FRotator Rotation;
		Rotation.Yaw = 0;
		Rotation.Pitch = 0;
		Rotation.Roll = 0;
		FActorSpawnParameters SpawnParams;
		FStringClassReference blackLinesWidgeClasstRef(Item_Ref.Use_Ref);
		UClass* blackLinesWidgetClass = blackLinesWidgeClasstRef.TryLoadClass<AUse_Item>();
		AUse_Item* Ref = GetWorld()->SpawnActor<AUse_Item>(blackLinesWidgetClass, this->GetActorLocation(), Rotation, SpawnParams);

		Ref->Count = Item_Ref.Count_Use;
		Ref->Start_Logic();
		if (Item_Ref.Count_Use == 1)
		{
				Items_System->Delete_Item(Item_Ref);
		}
		else
		{
			FI.Name = Item_Ref.Name;
			FI.Use_Ref = Item_Ref.Use_Ref;
			FI.Count_Use = Item_Ref.Count_Use - 1;
			Items_System->Change_Item(FI);
		}
	}
}

void AMT_TKCharacter::Set_Money(int I)
{
	Money = I;
}

void AMT_TKCharacter::Set_Was_Focus(bool B)
{
	Was_Focus = B;
}

bool AMT_TKCharacter::Get_Menu_Is_Open()
{
	return Menu_Is_Open;
}

int AMT_TKCharacter::Get_Money()
{
	return Money;
}

bool AMT_TKCharacter::Get_Was_Focus()
{
	return Was_Focus;
}

AShop* AMT_TKCharacter::Get_Shop_Ref()
{
	return Shop_Ref;
}

void AMT_TKCharacter::Set_Shop_Ref(AShop* A)
{
	Shop_Ref = A;
}

