// Fill out your copyright notice in the Description page of Project Settings.


#include "Item.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Components/InputComponent.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Item_Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Item_Mesh"));

	Item_Component = CreateDefaultSubobject<USceneComponent>(TEXT("Item_Component"));

	RootComponent = Item_Component;

	Item_Mesh->SetupAttachment(RootComponent);

	FVector Scale;
	Scale.X = 0.5;
	Scale.Y = 0.5;
	Scale.Z = 0.5;
	Item_Mesh->SetWorldScale3D(Scale);
	Item_Mesh->SetEnableGravity(true);
	Item_Mesh->SetSimulatePhysics(true);

	Item_Mesh->SetGenerateOverlapEvents(true);
	Item_Mesh->CanCharacterStepUpOn = ECB_Yes;
	Item_Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Item_Mesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Vehicle, ECollisionResponse::ECR_Overlap);
	Item_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Destructible, ECollisionResponse::ECR_Block);
	
	Data.Name ="";
	Data.Use_Ref = "";
	Data.Count_Use = 0;

}

void AItem::Interact(AActor* Interactor) //������ ��������� � ���������� �� � �����
{
	if (Cast<AMT_TKCharacter>(Interactor) != nullptr)
	{
		Cast<AMT_TKCharacter>(Interactor)->Items_System->Add_Item(Data);
	}
	
	Destroy();
}

UStaticMeshComponent* AItem::Get_Item_Mesh()
{
	return Item_Mesh;
}
