// Fill out your copyright notice in the Description page of Project Settings.


#include "Item_Speed_Up.h"

AItem_Speed_Up::AItem_Speed_Up()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Meshes(TEXT("'/Game/TopDownCPP/Blueprints/Drop_ICOSphere.Drop_ICOSphere'"));

	if (Meshes.Succeeded())
	{
		Item_Mesh->SetStaticMesh(Meshes.Object);
		Item_Mesh->SetRelativeLocation(FVector(70.f, 0.f, 0.f));
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Mats(TEXT("Material'/Game/StarterContent/Materials/M_Ground_Grass.M_Ground_Grass'"));

	if (Mats.Succeeded())
	{
		Item_Mesh->SetMaterial(0, Mats.Object);
	}

	
	Data.Name = "Speed";
	Data.Use_Ref = TEXT("Class'/Script/MT_TK.Use_Item_Speed_Up'");
	Data.Count_Use = 1;

}
