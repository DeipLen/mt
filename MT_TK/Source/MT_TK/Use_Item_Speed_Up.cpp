// Fill out your copyright notice in the Description page of Project Settings.

#include "Use_Item_Speed_Up.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MT_TKCharacter.h"

AUse_Item_Speed_Up::AUse_Item_Speed_Up()
{
	PrimaryActorTick.bCanEverTick = false;
	CountdownTime = 3;
}

 void AUse_Item_Speed_Up::Do_Something()
{
	// GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AUse_Item_Speed_Up::AdvanceTimer, 1.f, true);
	 GetWorld()->GetTimerManager().SetTimer(CountdownTimerHandle, this, &AUse_Item_Speed_Up::AdvanceTimer, 1.f, true);
	 GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("Start")));
	 AMT_TKCharacter* Char_Ref = Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	// UCharacterMovementComponent* MovementPtr = Cast<UCharacterMovementComponent>(Char_Ref->GetCharacterMovement());
	 Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->GetCharacterMovement()->MaxWalkSpeed = Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->GetCharacterMovement()->MaxWalkSpeed * 2;
}

 void AUse_Item_Speed_Up::AdvanceTimer()
 {
	 GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("...")));
	 --CountdownTime;
	 if (CountdownTime < 1)
	 {
		 GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
		 CountdownHasFinished();
	 }
 }

 void AUse_Item_Speed_Up::CountdownHasFinished()
 {
	 GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("End")));
	 Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->GetCharacterMovement()->MaxWalkSpeed = Cast<AMT_TKCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn())->GetCharacterMovement()->MaxWalkSpeed / 2;
	 Destroy();

 }

 void AUse_Item_Speed_Up::Start_Logic()
{
	Do_Something();
}