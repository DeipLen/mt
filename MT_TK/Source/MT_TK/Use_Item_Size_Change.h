// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Shop.h"
#include "MT_TKCharacter.h"
#include "CoreMinimal.h"
#include "Use_Item.h"
#include "Use_Item_Size_Change.generated.h"

/**
 * 
 */
UCLASS()
class MT_TK_API AUse_Item_Size_Change : public AUse_Item
{
	GENERATED_BODY()
	
public:
	AUse_Item_Size_Change();

	virtual void Start_Logic();

protected:

	virtual void Do_Something();
	


};
