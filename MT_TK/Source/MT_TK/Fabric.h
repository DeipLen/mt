// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Use_Item.h"
#include "CoreMinimal.h"

/**
 * 
 */
class MT_TK_API Fabric
{
public:
	Fabric();

	virtual AUse_Item* FactoryMethod() const = 0;
	void SomeOperation() const;
};
