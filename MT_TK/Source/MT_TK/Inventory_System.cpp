// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory_System.h"

// Sets default values for this component's properties
UInventory_System::UInventory_System()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventory_System::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInventory_System::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventory_System::Add_Item(FInventory_Item FI)
{
	Inventory.Add(FI);
}

FInventory_Item UInventory_System::Find_Item(FInventory_Item FI)
{
	for (int i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].Name == FI.Name)
		{
			return Inventory[i];
		}
	}
	FInventory_Item Not_Found;
	Not_Found.Name = "";
	Not_Found.Use_Ref = "";
	return Not_Found;
}

void UInventory_System::Delete_Item(FInventory_Item FI)
{
	for (int i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].Name == FI.Name)
		{
			Inventory.RemoveAt(i);
			return;
		}
	}
}

int UInventory_System::Count_Item(FInventory_Item FI)
{
	int Count = 0;
	for (int i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].Name == FI.Name)
		{
			Count++;
		}
	}
	return Count;
}


void UInventory_System::Change_Item(FInventory_Item FI)
{
	for (int i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].Name == FI.Name)
		{
			Inventory.RemoveAt(i);
			Inventory.Insert(FI, 0);
			return;
		}
	}
}

