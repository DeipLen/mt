// Fill out your copyright notice in the Description page of Project Settings.


#include "Coin.h"

// Sets default values
ACoin::ACoin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Coin_Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Coin_Mesh"));
	Coin_Component = CreateDefaultSubobject<USceneComponent>(TEXT("Coin_Component"));

	RootComponent = Coin_Component;
	Coin_Mesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Meshes(TEXT("'/Game/TopDownCPP/Blueprints/Coin.Coin'"));
	{
		Coin_Mesh->SetStaticMesh(Meshes.Object);
		Coin_Mesh->SetRelativeLocation(FVector(70.f, 0.f, 0.f));
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Mats(TEXT("Material'/Game/StarterContent/Materials/M_Metal_Gold.M_Metal_Gold'"));
	if (Mats.Succeeded())
	{
		Coin_Mesh->SetMaterial(0, Mats.Object);
	}

	FVector Scale;
	Scale.X = 0.5;
	Scale.Y = 0.5;
	Scale.Z = 0.5;
	Coin_Mesh->SetWorldScale3D(Scale);

	Coin_Mesh->SetEnableGravity(true);
	Coin_Mesh->SetSimulatePhysics(true);

	Coin_Mesh->SetGenerateOverlapEvents(true);
	Coin_Mesh->CanCharacterStepUpOn = ECB_Yes;
	Coin_Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Coin_Mesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Vehicle, ECollisionResponse::ECR_Overlap);
	Coin_Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Destructible, ECollisionResponse::ECR_Block);

	Cost = 100;
}

// Called when the game starts or when spawned
void ACoin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACoin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACoin::Interact(AActor* Interactor)
{
	if (Cast<AMT_TKCharacter>(Interactor) != nullptr)
	{
		Cast<AMT_TKCharacter>(Interactor)->Set_Money(Cost);
		Destroy();
	}
}

void ACoin::Set_Cost(int I)
{
	Cost = I;
}

int ACoin::Get_Cost()
{
	return Cost;
}