// Copyright Epic Games, Inc. All Rights Reserved.

#include "MT_TKPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "MT_TKCharacter.h"
#include "Engine/World.h"

AMT_TKPlayerController::AMT_TKPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AMT_TKPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AMT_TKPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("NAM_1", IE_Pressed, this, &AMT_TKPlayerController::NAM_1);
	InputComponent->BindAction("NAM_2", IE_Pressed, this, &AMT_TKPlayerController::NAM_2);
	InputComponent->BindAction("NAM_3", IE_Pressed, this, &AMT_TKPlayerController::NAM_3);

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AMT_TKPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AMT_TKPlayerController::OnSetDestinationReleased);
}

void AMT_TKPlayerController::NAM_1()
{
	
	if (Cast<AMT_TKCharacter>(GetPawn()) != nullptr)
	{
		Cast<AMT_TKCharacter>(GetPawn())->Use("Speed");
	}

}

void AMT_TKPlayerController::NAM_2()
{
	if (Cast<AMT_TKCharacter>(GetPawn()) != nullptr)
	{
		Cast<AMT_TKCharacter>(GetPawn())->Use("Three");
	}
}


void AMT_TKPlayerController::NAM_3()
{
	if (Cast<AMT_TKCharacter>(GetPawn()) != nullptr)
	{
		Cast<AMT_TKCharacter>(GetPawn())->Use("Size");
	}

}

void AMT_TKPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AMT_TKCharacter* MyPawn = Cast<AMT_TKCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void AMT_TKPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();

	if (Cast<AMT_TKCharacter>(MyPawn) != nullptr)
	{
		if (!Cast<AMT_TKCharacter>(MyPawn)->Get_Menu_Is_Open())
		{
			if (MyPawn)
			{
				float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

				// We need to issue move command only if far enough in order for walk animation to play correctly
				if ((Distance > 120.0f))
				{
					UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
				}
			}
		}
	}
}

void AMT_TKPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AMT_TKPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}
