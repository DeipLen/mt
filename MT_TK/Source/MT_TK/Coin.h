// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MT_TKCharacter.h"
#include "Interaction.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Coin.generated.h"

UCLASS()
class MT_TK_API ACoin : public AActor, public IInteraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* Coin_Mesh;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Coin_Component;

	UPROPERTY()
		int Cost;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
		void Set_Cost(int I);

	UFUNCTION()
		int Get_Cost();

};
