// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Use_Item.generated.h"

UCLASS()
class MT_TK_API AUse_Item : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUse_Item();

protected:

	UFUNCTION()
		virtual void Do_Something();
		
	

public:	

	UFUNCTION()
		virtual void Start_Logic();

	UPROPERTY()
		int Count;

};
