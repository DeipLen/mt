// Fill out your copyright notice in the Description page of Project Settings.


#include "Item_Size_Change.h"

AItem_Size_Change::AItem_Size_Change()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Meshes(TEXT("'/Game/TopDownCPP/Blueprints/Drop_Q.Drop_Q'"));

	if (Meshes.Succeeded())
	{
		Item_Mesh->SetStaticMesh(Meshes.Object);
		Item_Mesh->SetRelativeLocation(FVector(70.f, 0.f, 0.f));
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Mats(TEXT("Material'/Game/StarterContent/Materials/M_ColorGrid_LowSpec.M_ColorGrid_LowSpec'"));

	if (Mats.Succeeded())
	{
		Item_Mesh->SetMaterial(0, Mats.Object);
	}
	
	Data.Name = "Size";
	Data.Use_Ref = TEXT("Class'/Script/MT_TK.Use_Item_Size_Change'");
	Data.Count_Use = 2;
}