// Fill out your copyright notice in the Description page of Project Settings.


#include "Shop.h"

// Sets default values
AShop::AShop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Shop_Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Shop_Mesh"));
	Shop_Component = CreateDefaultSubobject<USceneComponent>(TEXT("Shop_Component"));

	RootComponent = Shop_Component;
	Shop_Mesh->SetupAttachment(RootComponent);

	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Meshes(TEXT("'/Game/TopDownCPP/Blueprints/vending_final.vending_final'"));
	{
		Shop_Mesh->SetStaticMesh(Meshes.Object);
		Shop_Mesh->SetRelativeLocation(FVector(70.f, 0.f, 0.f));
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Mats(TEXT("Material'/Game/StarterContent/Materials/M_Metal_Gold.M_Metal_Gold'"));
	if (Mats.Succeeded())
	{
		Shop_Mesh->SetMaterial(0, Mats.Object);
	}

	FVector Scale;
	Scale.X = 0.7;
	Scale.Y = 0.5;
	Scale.Z = 0.5;
	Shop_Mesh->SetWorldScale3D(Scale);

	Is_Start = false;

}

void AShop::Interact(AActor* Interactor) //����� ���� ��� ��������������
{
	AMT_TKCharacter* CST = Cast<AMT_TKCharacter>(Interactor);
	if (CST != nullptr)
	{
		if (!CST->Get_Was_Focus())
		{
			CST->Set_Was_Focus(true);
			CST->Set_Menu_Is_Open(true);
			if (!Is_Start)
			{
				Is_Start = true;
				FStringClassReference blackLinesWidgeClasstRef(TEXT("'/Game/TopDownCPP/Blueprints/BP_Menu.BP_Menu_C'"));
				UClass* blackLinesWidgetClass = blackLinesWidgeClasstRef.TryLoadClass<UMenu>();
				if (blackLinesWidgetClass)
				{
					Menu_Ref = CreateWidget<UMenu>(this->GetGameInstance(), blackLinesWidgetClass);
					if (Menu_Ref)
					{
						Menu_Ref->Set_Char_Ref(CST);
						Menu_Ref->Set_Shop_Ref(this);
						Menu_Ref->AddToViewport();
					}
				}
			}
		}
	}
}

void AShop::Set_Is_Start(bool B)
{
	Is_Start = B;
}

void AShop::Set_Menu_Ref(UMenu* UM)
{
	Menu_Ref = UM;
}

bool AShop::Get_Is_Start()
{
	return Is_Start;
}

UMenu* AShop::Get_Menu_Ref()
{
	return Menu_Ref;
}