// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Menu.h"
#include "MT_TKCharacter.h"
#include "Interaction.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shop.generated.h"

UCLASS()
class MT_TK_API AShop : public AActor, public IInteraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShop();

private:
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* Shop_Mesh;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Shop_Component;

	UPROPERTY()
		bool Is_Start;

	UPROPERTY()
		UMenu* Menu_Ref;

public:	

	virtual void Interact(AActor* Interactor);

	UFUNCTION()
		void Set_Is_Start(bool B);

	UFUNCTION()
		void Set_Menu_Ref(UMenu* UM);

	UFUNCTION()
		bool Get_Is_Start();

	UFUNCTION()
		UMenu* Get_Menu_Ref();
};
