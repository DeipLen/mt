// Copyright Epic Games, Inc. All Rights Reserved.

#include "MT_TKGameMode.h"
#include "MT_TKPlayerController.h"
#include "MT_TKCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMT_TKGameMode::AMT_TKGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMT_TKPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}