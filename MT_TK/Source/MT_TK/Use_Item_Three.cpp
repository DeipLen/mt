// Fill out your copyright notice in the Description page of Project Settings.


#include "Use_Item_Three.h"

AUse_Item_Three::AUse_Item_Three()
{
	PrimaryActorTick.bCanEverTick = true;
}

 void AUse_Item_Three::Do_Something()
{
}

 void AUse_Item_Three::Start_Logic()
{
	Do_Something();
	Destroy();
}
